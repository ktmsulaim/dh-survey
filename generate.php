<?php include_once('includes/header.php'); ?>
<?php
    $institutions = Institution::all();

?>

    <section id="generate" class="m-auto w-100">
        <div class="container mt-4 mb-4">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="mt-2 mb-0">Generate Reports</h4>
                        </div>
                        <div class="card-body">
                            <div class="sectios">
                                <div id="section1">
                                    <h5>Students by UG</h5>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <form action="report.php" method="get">
                                                <input type="hidden" name="section" value="1">
                                            <div class="md-form">
                                                <select id="ug" class="mdb-select md-form colorful-select dropdown-primary" name="ug" required searchable="Search here.." required>
                                                    <option value="" selected>Select UG</option>
                                                    <?php
                                                        if(count($institutions) > 0){
                                                            foreach($institutions as $inst){
                                                                echo '<option value="'.$inst->id.'" '.($insti == $inst->id ? 'selected' : '').'>'.$inst->name.'</option>';
                                                            }
                                                        }
                                                    ?>
                                                </select>
                                                <label for="ug" class="mdb-main label">Ug college</label>
                                            </div>
                                            <div class="md-form text-right">
                                                <button class="btn btn-primary btn-sm" name="get">GET</button>
                                            </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div id="section2">
                                    <h5>Section 1</h5>
                                    <h6>Q1. What is your passion?</h6>
                                    <div class="row mt-2">
                                        <div class="col-md-6">
                                            <form action="report.php" method="get">
                                                <input type="hidden" name="section" value="2">
                                                <input type="hidden" name="answer_num" value="1">
                                                <div class="md-form">
                                                    <select id="answer" class="mdb-select md-form colorful-select dropdown-primary" name="passion" searchable="Search here.." required>
                                                        <option value="" selected>Pick an answer</option>
                                                        <option value="Teacher">Teacher</option>
                                                        <option value="Orator">Orator</option>
                                                        <option value="Writer">Writer</option>
                                                        <option value="Trainer">Trainer</option>
                                                        <option value="Civil servant">Civil servant</option>
                                                        <option value="Khateeb">Khateeb</option>
                                                        <option value="Academician">Academician</option>
                                                        <option value="Businessman">Businessman</option>
                                                        <option value="Journalist">Journalist</option>
                                                        <option value="Job abroad">Job abroad</option>
                                                        <option value="IT expert">IT expert</option>
                                                        <option value="Government official">Government official</option>
                                                        <option value="Social servant">Social servant</option>
                                                        <option value="Not planned yet">Not planned yet</option>
                                                        <option value="others">Others</option>
                                                    </select>
                                                    <label for="answer" class="mdb-main label">Passion</label>
                                                </div>

                                                <h6 class="mt-2">Q2. Do you think you can make your passion come true?</h6>
                                                <input type="hidden" name="section" value="2">
                                                <input type="hidden" name="answer_num1" value="2">
                                                <div class="md-form">
                                                    <!-- Material inline 1 -->
                                                    <div class="form-check form-check-inline">
                                                    <input type="radio" class="form-check-input" id="no" name="sure" value="no" required>
                                                    <label class="form-check-label" for="no">No</label>
                                                    </div>

                                                    <!-- Material inline 2 -->
                                                    <div class="form-check form-check-inline">
                                                    <input type="radio" class="form-check-input" id="partially" value="partially" name="sure">
                                                    <label class="form-check-label" for="partially">Partially</label>
                                                    </div>

                                                    <!-- Material inline 3 -->
                                                    <div class="form-check form-check-inline">
                                                    <input type="radio" class="form-check-input" id="ofcourse" value="of course" name="sure">
                                                    <label class="form-check-label" for="ofcourse">Of course</label>
                                                    </div>
                                                </div>
                                                <div class="md-form text-right">
                                                    <button class="btn btn-primary btn-sm" name="get">GET</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div id="section3">
                                    <h5>Section 2</h5>
                                    <h6>Q1. What are you going to do next year after graduation?</h6>
                                    <div class="col-md-6">
                                        <form action="report.php" method="get">
                                            <input type="hidden" name="section" value="3">
                                            <input type="hidden" name="answer_num" value="1">
                                            <select id="answer1" class="mdb-select md-form colorful-select dropdown-primary" name="job" required searchable="Search here.." required>
                                                <option value="" selected>Pick an answer</option>
                                                <option value="Teacher in DH system">Teacher in DH system</option>
                                                <option value="Teacher outside the system">Teacher outside the system</option>
                                                <option value="Job in Mahallu (as khateeb, coordinator, or others )">Job in Mahallu (as khateeb, coordinator, or others)</option>
                                                <option value="Go for a job abroad">Go for a job abroad</option>
                                                <option value="Go for higher studies">Go for higher studies</option>
                                                <option value="Do another job">Do another job</option>
                                            </select>
                                            <div class="md-form text-right">
                                                <button class="btn btn-primary btn-sm" name="get">GET</button>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- question 2 -->
                                    <h6>Q2. Are you interested in Civil service practices?</h6>
                                    <div class="col-md-6">
                                        <form action="report.php" method="get">
                                            <input type="hidden" name="section" value="3">
                                            <input type="hidden" name="answer_num" value="2">
                                            <div class="answer2">
                                                <div class="select">
                                                    <!-- Material inline 1 -->
                                                    <div class="form-check form-check-inline">
                                                    <input type="radio" class="form-check-input" id="answer2_yes" name="civil" value="Yes" checked>
                                                    <label class="form-check-label" for="answer2_yes">Yes</label>
                                                    </div>

                                                    <!-- Material inline 2 -->
                                                    <div class="form-check form-check-inline">
                                                    <input type="radio" class="form-check-input" id="answer2_no" name="civil" value="No">
                                                    <label class="form-check-label" for="answer2_no">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="md-form text-right">
                                                <button class="btn btn-primary btn-sm" name="get">GET</button>
                                            </div>
                                        </form>
                                    </div>

                                    <!-- question 3 -->
                                    <h6>Q3. Are interested in working for Hadia projects out of the state?</h6>
                                    <div class="col-md-6">
                                        <form action="report.php" method="get">
                                            <input type="hidden" name="section" value="3">
                                            <input type="hidden" name="answer_num" value="3">
                                            <div class="answer2">
                                                <div class="select">
                                                    <!-- Material inline 1 -->
                                                    <div class="form-check form-check-inline">
                                                    <input type="radio" class="form-check-input" id="answer3_yes" name="project" value="Yes" checked>
                                                    <label class="form-check-label" for="answer3_yes">Yes</label>
                                                    </div>

                                                    <!-- Material inline 2 -->
                                                    <div class="form-check form-check-inline">
                                                    <input type="radio" class="form-check-input" id="answer3_no" name="project" value="No">
                                                    <label class="form-check-label" for="answer3_no">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="md-form text-right">
                                                <button class="btn btn-primary btn-sm" name="get">GET</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <hr>

                                <div id="section4">
                                    <h5>Section 3</h5>
                                    <h6>Skill wise students</h6>
                                    <div class="col-md-6">
                                        <form action="report.php" method="get">
                                            <input type="hidden" name="section" value="4">
                                            <div class="form-group">
                                                <select name="skill" id="skill" class="mdb-select md-form colorful-select dropdown-primary" required>
                                                    <option value="">Select a skill</option>
                                                    <option value="art">Art</option>
                                                    <option value="writing">Writing</option>
                                                    <option value="presentation">Presentation</option>
                                                    <option value="coordination">Coordination</option>
                                                    <option value="leadership">Leadership</option>
                                                    <option value="kithab">Kithab</option>
                                                    <option value="maths">Maths</option>
                                                    <option value="gk">General knowledge</option>
                                                    <option value="it">Information Technology</option>
                                                    <option value="lang">Language abilities</option>
                                                </select>
                                            </div>
                                            <div class="md-form text-right">
                                                <button class="btn btn-primary btn-sm" name="get">GET</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <hr>

                                <div id="section4">
                                <?php
                                    $total_feedback = count(Feedback::all());
                                ?>
                                    <h5>Section 4</h5>
                                    <h6>Feedback (<?php echo $total_feedback; ?>)</h6>
                                    <b class="display-4"><?php $feedback = Feedback::avg(); echo number_format($feedback->rating, 1); ?> / 5</b><br>
                                    <p>Rating:</p>
                                    <div class="col-md-6">
                                    <ul class="list-group">
                                        <li class="list-group-item"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>  <span class="ml-2"> - <?php echo Feedback::getByRating(5); ?></span></li>
                                        <li class="list-group-item"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"> <span class="ml-2"> - <?php echo Feedback::getByRating(4); ?></span></i></li>
                                        <li class="list-group-item"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <span class="ml-2"> - <?php echo Feedback::getByRating(3); ?></span></li>
                                        <li class="list-group-item"><i class="fa fa-star"></i><i class="fa fa-star"></i> <span class="ml-2"> - <?php echo Feedback::getByRating(2); ?></span></li>
                                        <li class="list-group-item"><i class="fa fa-star"></i> <span class="ml-2"> - <?php echo Feedback::getByRating(1); ?></span></li>
                                    </ul>
                                        <form action="report.php" method="get">
                                            <input type="hidden" name="section" value="4">
                                            <input type="hidden" name="feedback">
                                            <div class="md-form text-right">
                                                <button class="btn btn-primary btn-sm" name="get">GET</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php include_once('includes/footer.php'); ?>

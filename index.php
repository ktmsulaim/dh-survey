<?php include_once('includes/header.php'); ?>
<?php if($stdsession->isVerified()){}else{ redirect("login.php"); } ?>
<?php

    $student_id = Student::studentByEnroll($_SESSION['enroll_no']);
    $student = Student::findById($student_id);
    $institutions = Institution::all();

    // get answer1 from section 2
    $answer2 = Answer::getBySection($student->id, 3);
    $status = 0;
    $answers = array();
    if($answer2->num_rows > 0){
        foreach($answer2 as $ans){
            $answer_number = $ans['answer_number'];
            $answer = $ans['answer'];

            array_push($answers, $answer);
            
        }

        if(empty($answers[0])){
            $status = 0;
        }else{
            $status = 1;
        }
    }

  if(Student::hasStep1($student->id)){
    if($status == 1){
        if(Student::checkAll($student->id) > 0){
            redirect("finish.php");
        }
    }else{
        redirect("step3.php");
    }
  }

    if(isset($_POST['next'])){
        $inst = trim($_POST['ug']);
        if(Student::hasStep1($student->id)){
            if(Student::updateInst($student->id, $inst)){
                redirect("step2.php");
            }else{
                redirect($_SERVER['PHP_SELF']."?failed");
            }
        }else{
            if(Student::setInst($student->id, $inst)){
                redirect("step2.php");
            }else{
                redirect($_SERVER['PHP_SELF']."?failed");
            }
        }
    }

    if(Student::hasStep1($student->id)){
        $insti = Student::getInst($student->id);
    }



?>
    <div class="container m-auto">
        <div class="row">
            <div class="col-md-5 col-sm-10 col-xs-12 m-auto">
                <form action="" method="POST">
                <section class="mb-5">

                    <!-- Card -->
                    <div class="card card-cascade narrower">

                    <!-- Card image -->
                    <div class="view view-cascade text-center bg-white">
                        <h2><br> HUDAWI <sup class="text-danger"><strong>+</strong></sup><br></h2>
                        <div class="progress md-progress mt-3">
                            <div class="progress-bar" role="progressbar" style="width: 10%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <!-- /Card image -->

                    <!-- Card content -->
                    <div class="card-body card-body-cascade text-center table-responsive">

                        <!-- Horizontal Steppers -->
                        <div class="row">
                            <div class="col-md-12">
                                <p class="h5 text-center mb-4 step-head">Personal Info</p>

                                <div class="quote">
                                    <blockquote class="blockquote">
                                    “Knowing yourself is the beginning of all wisdom.”
                                    <footer class="blockquote-footer mb-3 text-right">Aristotle</footer>

                                    </blockquote>
                                </div>

                                <div class="md-form">
                                    <input type="text" id="name" name="name" class="form-control" value="<?php echo $student->name; ?>">
                                    <label for="name" class="">Name</label>
                                </div>

                                <div class="md-form">
                                    <select id="ug" class="mdb-select md-form colorful-select dropdown-primary" name="ug" required searchable="Search here.." required>
                                        <option value="" selected>Select UG</option>
                                        <?php
                                            if(count($institutions) > 0){
                                                foreach($institutions as $inst){
                                                    echo '<option value="'.$inst->id.'" '.($insti == $inst->id ? 'selected' : '').'>'.$inst->name.'</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                    <label for="ug" class="mdb-main label">Ug college</label>
                                </div>

                                <div class="text-center mt-4">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light" name="next">Next</button>
                                    <!-- <input type="submit" class="btn btn-primary waves-effect waves-light" name="next" value="Next"> -->
                                </div>
                                <div class="logout">
                                    It wasn't you? <a href="logout.php">Sign out</a>
                                </div>
                            </div>
                        </div>
                        <!-- Horizontal Steppers -->

                    </div>
                    <!-- Card content -->

                    </div>
                    <!-- Card -->

                    </section>
                </form>
            </div>
        </div>
    </div>
 

<!-- footer -->
<?php include_once('includes/footer.php'); ?>
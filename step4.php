<?php include_once('includes/header.php'); ?>
<?php if($stdsession->isVerified()){}else{ redirect("login.php"); } ?>
<?php


    $student_id = Student::studentByEnroll($_SESSION['enroll_no']);
    $student = Student::findById($student_id);

    
    if(Student::checkAll($student->id) > 0){
        redirect("finish.php");
    }

    // get all selected skills
    $skills = Skill::findByStudentId($student->id);

    $skillArr = array();

    if($skills){
        if(count($skills) > 0){
            foreach($skills as $skill){
                array_push($skillArr, $skill->skill);
            }
        }
    }

    if(isset($_POST['next'])){
       if(empty($_POST['skills']) && $_POST['skills'] !== ''){
            
            if(Student::hasStep4($student->id)){
                $skills = Skill::findByStudentId($student->id);
                if($skills){
                    if(count($skills) > 0){
                        foreach($skills as $skill){
                            $skill->delete();
                        }
                    }
                }

                $skills = $_POST['skill'];
                foreach($skills as $skill){
                    $skillObj = new Skill();
                    $skillObj->student_id = $student_id;
                    $skillObj->skill = $skill;
                    
                    if($skillObj->save()){
                        redirect("step5.php");
                    }else{
                        redirect($_SERVER['PHP_SELF']."?failed");
                    }
                }
            }else{
                $skills = $_POST['skill'];
                foreach($skills as $skill){
                    $skillObj = new Skill();
                    $skillObj->student_id = $student_id;
                    $skillObj->skill = $skill;
                    
                    if($skillObj->save()){
                        redirect("step5.php");
                    }else{
                        redirect($_SERVER['PHP_SELF']."?failed");
                    }
                }
            }
       }else{
           redirect('step4.php?empty');
       }
    }

?>
    <div class="container m-auto">
        <div class="row">
            <div class="col-md-9 col-sm-10 col-xs-12 m-auto">
                <form action="" method="POST">
                <section class="mb-5">

                    <!-- Card -->
                    <div class="card card-cascade narrower">

                    <!-- Card image -->
                    <div class="view view-cascade text-center bg-white">
                        <h2><br> HUDAWI <sup class="text-danger"><strong>+</strong></sup><br></h2>
                        <div class="progress md-progress mt-3">
                            <div class="progress-bar" role="progressbar" style="width: 75%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <!-- /Card image -->

                    <!-- Card content -->
                    <div class="card-body card-body-cascade text-center table-responsive">

                        <!-- Horizontal Steppers -->
                        <div class="row">
                            <div class="col-md-12">
                                <p class="h5 text-center mb-4 step-head">Step 3</p>


                                <div class="md-form text-left">
                                    <strong>The skills you bagged through the twelve years? </strong><br><span>(You are permitted to choose more than one,<span class="text-danger"> only if you are confident</span>)</span>
                                    <div class="options">
                                        <div class="row clearfix">
                                            <div class="col-md-6 col-xs-12 col-sm-6">
                                                <!-- Material unchecked -->
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" id="art" value="Art" name="skill[]" <?php echo in_array('Art', $skillArr) ? 'checked' : ''; ?>>
                                                    <label class="form-check-label" for="art">Art</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-xs-12 col-sm-6">
                                                <!-- Material unchecked -->
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" id="writing" value="Writing" name="skill[]" <?php echo in_array('Writing', $skillArr) ? 'checked' : ''; ?>>
                                                    <label class="form-check-label" for="writing">Writing</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-xs-12 col-sm-6">
                                                <!-- Material unchecked -->
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" id="presentation" value="Presentation" name="skill[]" <?php echo in_array('Presentation', $skillArr) ? 'checked' : ''; ?>>
                                                    <label class="form-check-label" for="presentation">Presentation</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-xs-12 col-sm-6">
                                                <!-- Material unchecked -->
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" id="coordination" value="Coordination" name="skill[]" <?php echo in_array('Coordination', $skillArr) ? 'checked' : ''; ?>>
                                                    <label class="form-check-label" for="coordination">Coordination</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-xs-12 col-sm-6">
                                                <!-- Material unchecked -->
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" id="leadership" value="Leadership" name="skill[]" <?php echo in_array('Leadership', $skillArr) ? 'checked' : ''; ?>>
                                                    <label class="form-check-label" for="leadership">Leadership</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-xs-12 col-sm-6">
                                                <!-- Material unchecked -->
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" id="kithab" value="Kithab" name="skill[]" <?php echo in_array('Kithab', $skillArr) ? 'checked' : ''; ?>>
                                                    <label class="form-check-label" for="kithab">Kithab</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-xs-12 col-sm-6">
                                                <!-- Material unchecked -->
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" id="Maths" value="Maths" name="skill[]" <?php echo in_array('Maths', $skillArr) ? 'checked' : ''; ?>>
                                                    <label class="form-check-label" for="Maths">Maths</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-xs-12 col-sm-6">
                                                <!-- Material unchecked -->
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" id="gk" value="General knowledge" name="skill[]" <?php echo in_array('General knowledge', $skillArr) ? 'checked' : ''; ?>>
                                                    <label class="form-check-label" for="gk">General knowledge</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-xs-12 col-sm-6">
                                                <!-- Material unchecked -->
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" id="it" value="IT" name="skill[]" <?php echo in_array('IT', $skillArr) ? 'checked' : ''; ?>>
                                                    <label class="form-check-label" for="it">Information Technology</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-xs-12 col-sm-6">
                                                <!-- Material unchecked -->
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" id="lang" value="Language abilities" name="skill[]" <?php echo in_array('Language abilities', $skillArr) ? 'checked' : ''; ?>>
                                                    <label class="form-check-label" for="lang">Language abilities</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               

                                <div class="text-center mt-4">
                                    <a href="step3.php" class="btn btn-primary waves-effect waves-light">Back</a>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light" name="next">Next</button>
                                </div>
                            </div>
                        </div>
                        <!-- Horizontal Steppers -->

                    </div>
                    <!-- Card content -->

                    </div>
                    <!-- Card -->

                    </section>
                </form>
            </div>
        </div>
    </div>
 

<!-- footer -->
<?php include_once('includes/footer.php'); ?>
<script>
    $(function(){
       
    });
</script>
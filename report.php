<?php include_once('includes/header.php'); ?>
<?php
    if(isset($_GET['section'])){
        if(!empty($_GET['section'])){
            // title name
            $title = "";
            $count = "";
            $desc = "";
            if(isset($_GET['ug']) && !empty($_GET['ug'])){
                $title = "UG wise students";
                $students = Student::studentsByInstitute($_GET['ug']);
                $count = '<span class="badge badge-primary ml-2">'.count($students).'</span>';
                $desc = Institution::findById($_GET['ug']);
                $desc = $desc->name;
            }elseif(isset($_GET['passion']) && !empty($_GET['passion']) && isset($_GET['sure']) && !empty($_GET['sure'])){
                $title = "Passion wise students";
                $students = Student::sectionone($_GET['passion'], $_GET['sure']);
                $desc = $_GET['passion'] . ' - '.ucfirst($_GET['sure']);
                $count = '<span class="badge badge-primary ml-2">'.count($students).'</span>';
            }elseif(isset($_GET['job']) && !empty($_GET['job'])){
                $title = "Job wise students";
                $desc = $_GET['job'];
                if($_GET['job'] == 'Teacher in DH system' || $_GET['job'] == 'Teacher outside the system'){
                    $desc .= ' ('.(!empty($_GET['years']) ? $_GET['years'] : 'All').')';
                }
                

                if(isset($_GET['sub']) && !empty($_GET['sub'])){
                    $students = Student::sectionTwoConditional($_GET['job'], $_GET['sub']);
                    $desc .= ' ('.(!empty($_GET['sub']) ? $_GET['sub'] : '').')';
                }else{
                    $students = Student::sectiontwo(1, $_GET['job']);
                }

                $count = '<span class="badge badge-primary ml-2">'.count($students).'</span>';
            }elseif(isset($_GET['civil']) && !empty($_GET['civil'])){
                $title = "Students interested in Civil service practices";
                $desc = ucfirst($_GET['civil']);
                $students =  Student::sectiontwo(2, $_GET['civil']);
                $count = '<span class="badge badge-primary ml-2">'.count($students).'</span>';
            }elseif(isset($_GET['project']) && !empty($_GET['project'])){
                $title = "Students interested in working for Hadia projects";
                $desc = ucfirst($_GET['project']);
                $students = Student::sectiontwo(3, $_GET['project']);
                $count = '<span class="badge badge-primary ml-2">'.count($students).'</span>';
            }


            if(isset($_GET['skill']) && !empty($_GET['skill'])){
                $title = "Students by skills";
                $desc = ucfirst($_GET['skill']);
                if($_GET['skill'] == 'it'){
                    $desc = 'Information Technology';
                }elseif($_GET['skill'] == 'gk'){
                    $desc = 'General knowledge';
                }elseif($_GET['skill'] == 'lang'){
                    $desc = 'Language abilities';
                }
                $students = Student::studentsBySkill($_GET['skill']);
                $count = '<span class="badge badge-primary ml-2">'.count($students).'</span>';
            }

            if(isset($_GET['feedback'])){
                $title = "Feedback";
                $desc = "";
                $students = Student::studentsWithFeedback();
                $count = '<span class="badge badge-primary ml-2">'.count($students).'</span>';
            }
            
        }else{
            redirect("generate.php?emptySection");
        }
    }else{
        redirect("generate.php?selectSection");
    }
?>



<section id="report" class="w-100 m-auto">
    <div class="container  mt-4 mb-4">
        
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="mt-2 mb-0"><?php echo $title.$count; ?> <a href="generate.php" class="float-right small text-muted">Back</a></h4>
                        <p class="m-0"><?php echo !empty($desc) ? $desc : ''; ?></p>
                    </div>
                    <div class="card-body">
                    
                        <?php
                            // UG wise students
                            if(isset($_GET['section']) && !empty($_GET['section'])){ 
                                if(isset($_GET['job']) && !empty($_GET['job'])){
                                    if($_GET['job'] == 'Teacher in DH system' || $_GET['job'] == 'Teacher outside the system'){
                                        echo '<form action="" method="get">
                                        <input type="hidden" name="section" value="3">
                                        <input type="hidden" name="answer_number" value="1">
                                        <input type="hidden" name="job" value="'.$_GET['job'].'">
                                        <div class="md-form mb-3">
                                            <div class="form-row">
                                                <div class="col-md-1">
                                                    Filter:
                                                </div>
                                                <div class="col-md-10">
                                                    <select id="term" class="mdb-select colorful-select dropdown-primary" name="sub">
                                                        <option value="" selected>All</option>
                                                        <option value="Long term">Long term (More than five years)</option>
                                                        <option value="Short time">Short time (Compulsory 2 years)</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-1">
                                                    <span class="input-group-btn">
                                                        <button type="submit" class="btn btn-default btn-sm">Go!</button>
                                                    </span>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </form>';   
                                    }
                                }    
                            ?>
                            
                                <table class="table table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Enroll.No</th>
                                            <th>Name</th>
                                            <?php
                                                if(isset($_GET['passion']) && !empty($_GET['passion'])){
                                                    if($_GET['passion'] == 'others'){
                                                        echo '<th>Passion</th>';
                                                    }
                                                }elseif(isset($_GET['job']) && !empty($_GET['job'])){
                                                    if($_GET['job'] == 'Go for higher studies'){
                                                        echo '<th>When</th>
                                                        <th>Subject</th>
                                                        <th>Subject 2</th>
                                                        <th>University</th>';
                                                    }elseif($_GET['job'] == 'Do another job'){
                                                        echo '<th>Job</th>';
                                                    }
                                                }elseif(isset($_GET['feedback'])){
                                                    echo '<th>Rating</th>';
                                                    echo '<th>Feedback</th>';
                                                }
                                            ?>
                                            <th>Semester</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            if(count($students) > 0){
                                                $i = 1;
                                                foreach($students as $student) : ?>
                                                <tr>
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo $student->enroll_no; ?></td>
                                                    <td><?php echo $student->name; ?></td>
                                                    <?php
                                                        if(isset($_GET['passion']) && !empty($_GET['passion'])){
                                                            if($_GET['passion'] == 'others'){
                                                                $answer = Answer::answerByStudentId($student->id, 2, 1);
                                                                if($answer->num_rows > 0){
                                                                    foreach($answer as $ans){
                                                                        $answerId = $ans['id'];
                                                                        $answer = $ans['answer'];
                                                                    }

                                                                    $passion = Sub_answer::findByAnswerId($answerId);
                                                                    $passion = $passion->answer;

                                                                    echo '<td>'.$passion.'</td>';
                                                                }else{
                                                                    echo '<td></td>';
                                                                }
                                                                
                                                            }
                                                        }elseif(isset($_GET['job']) && !empty($_GET['job'])){
                                                            if($_GET['job'] == 'Go for higher studies'){
                                                                $answer = Answer::answerByStudentId($student->id, 3, 1);
                                                                if($answer->num_rows > 0){
                                                                    foreach($answer as $ans){
                                                                        $answerId = $ans['id'];
                                                                        $answer = $ans['answer'];
                                                                    }

                                                                    $answers = array();
                                                                    $sub_answers = Sub_answer::getByAnswerId($answerId);
                                                                    if(count($sub_answers) > 0){
                                                                        foreach($sub_answers as $key => $sub_answer){
                                                                            array_push($answers, $sub_answer->answer);
                                                                        }

                                                                        $when = $answers[0];
                                                                        $subject = $answers[1];
                                                                        $subject2 = $answers[1] == 'others' ? $answers[2] : '';
                                                                        $university = $answers[1] == 'others' ? $answers[3] : $answers[2];

                                                                        echo '<td>'.$when.'</td>';
                                                                        echo '<td>'.$subject.'</td>';
                                                                        echo '<td>'.$subject2.'</td>';
                                                                        echo '<td>'.$university.'</td>';
                                                                    }
                                                                }
                                                            }elseif($_GET['job'] == 'Do another job'){
                                                                $answer = Answer::answerByStudentId($student->id, 3, 1);
                                                                if($answer->num_rows > 0){
                                                                    foreach($answer as $ans){
                                                                        $answerId = $ans['id'];
                                                                        $answer = $ans['answer'];
                                                                    }

                                                                    $sub_answers = Sub_answer::getByAnswerId($answerId);

                                                                    if(count($sub_answers) > 0){
                                                                        foreach($sub_answers as $sub_answer){
                                                                            echo '<td>'.$sub_answer->answer.'</td>';
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }elseif(isset($_GET['feedback'])){
                                                            $feedback = Feedback::getByStudentId($student->id);
                                                            if(count($feedback) > 0){
                                                                foreach($feedback as $feed){
                                                                    echo '<td>'.$feed->rating.'</td>';
                                                                    echo '<td>'.$feed->feedback.'</td>';
                                                                }
                                                            }
                                                        }
                                                    ?>
                                                    <td><?php echo $student->semester; ?></td>
                                                </tr>
                                            <?php  $i++;  endforeach;
                                            }else{ ?>
                                                <tr>
                                                    <td colspan="4">No data found!</td>
                                                </tr>
                                        <?php    }
                                        ?>
                                    </tbody>
                                </table>
                         <?php   }
                        ?>
                    </div>
                </div>                
            </div>
            
        </div>
            
    </div>
</section>

<?php include_once('includes/footer.php'); ?>
<script src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.print.min.js"></script>

<script>
 $(document).ready(function() {
    $('.dataTable').DataTable( {
        dom: 'Bfrtip',
        buttons: [
                {
                    extend: 'excelHtml5',
                    title: '<?php echo $title.' - '.$desc; ?>'
                },
                {
                    extend: 'pdfHtml5',
                    title: '<?php echo $title.' - '.$desc; ?>'
                }
        ]
    } );

    $('.dt-button').removeClass('dt-button buttons-copy buttons-html5').addClass('btn-sm btn-primary btn');
} );
</script>
<?php include_once('includes/header.php'); ?>
<?php if($stdsession->isVerified()){}else{ redirect("login.php"); } ?>
<?php

    $student_id = Student::studentByEnroll($_SESSION['enroll_no']);
    $student = Student::findById($student_id);

    $answer1 = Answer::answerbyStudentId($student->id, 2, 1);
    $answer2 = Answer::answerbyStudentId($student->id, 2, 2);
    $answer3 = Answer::answerbyStudentId($student->id, 3, 1);
    $answer4 = Answer::answerbyStudentId($student->id, 3, 2);
    $answer5 = Answer::answerbyStudentId($student->id, 3, 3);

    $rs_answer1 = '';
    $rs_answer2 = '';
    $rs_answer3 = '';
    $rs_answer4 = '';

    // Fist answer of question
    
    if($answer1->num_rows >0){
        foreach($answer1 as $ans1){
            $rs_answer1 = $ans1['answer'];
        }
    } 
    // Second answer of question
    if($answer2->num_rows >0){
        foreach($answer2 as $ans2){
            $rs_answer2 = $ans2['answer'];
            
        } 
    }
    // Third answer
    if($answer3->num_rows > 0){
        foreach($answer3 as $ans3){
            $rs_answer3 = $ans3['answer'];
        } 
    }
    // find sub answer also
    $sub_answer1 = Sub_answer::getByAnswerId($ans3['id']);

    // Fourth answer
    if($answer4->num_rows > 0){
        foreach($answer4 as $ans4){
            $rs_answer4 = $ans4['answer'];
        } 
    }
    // Fifith answer
    if($answer5->num_rows > 0){
        foreach($answer5 as $ans5){
            $rs_answer5 = $ans5['answer'];
        } 
    }

    // skills
    $skills = Skill::findByStudentId($student->id);

    // feedback
    $feedbacks = Feedback::getByStudentId($student->id);
    if(count($feedbacks) > 0){
        foreach($feedbacks as $feedback){
            $rating = $feedback->rating;
            $fd = $feedback->feedback;
        }
    } 
?>
    <style>
        .list-circle{
            list-style-type: circle;
        }
    </style>
    <div class="container m-auto">
        <div class="row">
            <div class="col-md-9 col-sm-10 col-xs-12 m-auto">
                <form action="" method="POST">
                <section class="mb-5">

                    <!-- Card -->
                    <div class="card card-cascade narrower">

                    <!-- Card image -->
                    <div class="view view-cascade text-center bg-white">
                        <h2><br> HUDAWI <sup class="text-danger"><strong>+</strong></sup><br><br></h2>
                    </div>
                    <!-- /Card image -->

                    <!-- Card content -->
                    <div class="card-body card-body-cascade text-center table-responsive">

                        <!-- Horizontal Steppers -->
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="h5 text-center mb-4 clearfix">View response <span class="float-right"><a class="text-danger" href="logout.php">Sign out</a></span></p>
                                    <!-- Timeline -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="list-group text-left">
                                                <ul class="list-group">
                                                    <li class="list-group-item">Name: <?php echo $student->name; ?></li>
                                                    <li class="list-group-item">Enroll.No: <?php echo $student->enroll_no; ?></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                    <div class="col-md-12">
                                        <div class="timeline-main">
                                        <!-- Timeline Wrapper -->
                                        <ul class="stepper stepper-vertical timeline timeline-basic pl-0">

                                            <li>
                                            <!--Section Title -->
                                            <a href="#!">
                                                <span class="circle primary-color z-depth-1-half">1</span>
                                            </a>

                                            <!-- Section Description -->
                                            <div class="step-content z-depth-1 ml-2 p-4">
                                                <h5 class="">What is your passion?</h5>
                                                <p class="font-weight-bold"><?php echo $rs_answer1; ?></p>
                                                <hr>
                                                <h5 class="">Do you think you can make your passion come true?</h5>
                                                <p class="font-weight-bold"><?php echo $rs_answer2; ?></p>
                                            </div>
                                            </li>
                                            <li class="timeline-inverted">
                                            <!--Section Title -->
                                            <a href="#!">
                                                <span class="circle primary-color z-depth-1-half">2</span>
                                            </a>

                                            <!-- Section Description -->
                                            <div class="step-content z-depth-1 mr-xl-2 p-4">
                                                <h5 class="mb-3">What are you going to do next year after graduation?</h5>
                                                <p class="font-weight-bold"><?php echo $rs_answer3; ?></p>
                                                <?php 
                                                    
                                                    if(count($sub_answer1) > 1){
                                                        $i = 1;
                                                        $q = '';
                                                        foreach($sub_answer1 as $sb1){
                                                            if($rs_answer3 == 'Go for higher studies'){
                                                                if($i = 2){
                                                                    $q = 'Which subject?';
                                                                }elseif($sb1->answer == 'others'){
                                                                    if($i = 3){
                                                                        $q = 'Else what?';
                                                                    }else{
                                                                        $q = '';
                                                                    }
                                                                    
                                                                }elseif($i = 4){
                                                                    $q = 'The institution where you want to be enrolled:';
                                                                }elseif($i = 1){
                                                                    $q = 'When you want to go?';
                                                                }
                                                            }
                                                            echo '<p class="mt-2 mb-0 font-weight-bold text-left"><span class="mr-3 font-weight-normal">'.$q.'</span><br>'.$sb1->answer.'</p>';
                                                            
                                                            $i++;
                                                        }
                                                    }else{
                                                        foreach($sub_answer1 as $sb1){
                                                            echo '<p class="mb-0 font-weight-bold">'.$sb1->answer.'</p>';
                                                        }
                                                    }
                                                ?>
                                                <hr>
                                                <h5 class="mb-3">Are you interested in Civil service practices?</h5>
                                                <p class="font-weight-bold"><?php echo $rs_answer4; ?></p>
                                                <hr>
                                                <h5 class="mb-3">Are interested in working for Hadia projects out of the state?</h5>
                                                <p class="font-weight-bold"><?php echo $rs_answer5; ?></p>
                                            </div>
                                            </li>
                                            <li>
                                            <!--Section Title -->
                                            <a href="#!">
                                                <span class="circle primary-color z-depth-1-half">3</span>
                                            </a>

                                            <!-- Section Description -->
                                            <div class="step-content z-depth-1 ml-2 p-4">
                                                <h5 class="mb-3">The skills you bagged through the twelve years</h5>
                                                <div class="numbered text-left">
                                                    <?php
                                                        if(count($skills) > 0){
                                                            $i = 1;
                                                            foreach($skills as $skill){
                                                                echo '<span>'.$i.' - '.$skill->skill.'</span><br>';

                                                                $i++;
                                                            }
                                                        }
                                                    ?>
                                                </div>
                                            </div>
                                            </li>
                                            <li class="timeline-inverted">
                                            <!--Section Title -->
                                            <a href="#!">
                                                <span class="circle primary-color z-depth-1-half">4</span>
                                            </a>

                                            <!-- Section Description -->
                                                <div class="step-content z-depth-1 mr-xl-2 p-4">
                                                    <h5 class="">What problems and defects you see in DH PG system?</h5>
                                                    <p class="mt-4"><?php echo $fd; ?></p>
                                                    <hr>
                                                    <h5>Your rating</h5>
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div id="rateMe" class="rating-faces"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <!-- Timeline Wrapper -->
                                        </div>
                                    </div>
                                    </div>
                                    <!-- Timeline -->
                                </div>
                            </div>
                        </div>
                        <!-- Horizontal Steppers -->

                    </div>
                    <!-- Card content -->

                    </div>
                    <!-- Card -->

                    </section>
                </form>
            </div>
        </div>
    </div>


<!-- footer -->
<?php include_once('includes/footer.php'); ?>
<script>
$(function(){
    $("#rateMe").emotionsRating({
    <?php
        if($rating == 1){
            echo 'bgEmotion: "angry",';
        }elseif($rating == 2){
            echo 'bgEmotion: "disappointed",';
        }elseif($rating == 3){
            echo 'bgEmotion: "meh",';
        }elseif($rating == 4){
            echo 'bgEmotion: "happy",';
        }elseif($rating == 5){
            echo 'bgEmotion: "inLove",';
        }
    ?>

    // emotions: ['angry','disappointed','meh'],

    count: <?php echo $rating; ?>,

    // #color of emoji
    // #gold, red, blue, green, black,
    // #brown, pink, purple, orange
    color: "blue",

    // #size of emoji
    emotionSize: 30,

    // #input name
    inputName: "rating"
    });
});
</script>
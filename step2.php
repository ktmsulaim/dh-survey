<?php include_once('includes/header.php'); ?>
<?php if($stdsession->isVerified()){}else{ redirect("login.php"); } ?>
<?php

    $student_id = Student::studentByEnroll($_SESSION['enroll_no']);
    $student = Student::findById($student_id);

    if(Student::checkAll($student->id) > 0){
        redirect("finish.php");
    }

    $answd1 = "";
    $answd2 = "";
    $sub1 = "";
    
     // Retirieving answers
     $answered1 = Answer::answerByStudentId($student->id, 2, 1);
     $answered2 = Answer::answerByStudentId($student->id, 2, 2);
 
     if($answered1->num_rows > 0){
         foreach($answered1 as $ansd1){
             $answd1id = $ansd1['id'];
             $answd1 = $ansd1['answer'];
         }
     }
 
     if($answd1 == 'others'){
         $sub1 = Sub_answer::getByAnswerId($answd1id);
     }
     if($answered2->num_rows > 0){
         foreach($answered2 as $ansd2){
             $answd2 = $ansd2['answer'];
         }
     }

    //  Saving data

    if(isset($_POST['next'])){
        $answer1 = $_POST['answer1'];
        $answer2 = $_POST['answer2'];

        if(Student::hasStep2($student->id)){
            if($_POST['answer1'] == 'others'){
                $sub_answer1 = $_POST['answer1sub'];
            }

                // Retrieving saved answers
                $answer = Answer::answerByStudentId($student->id, 2, 1);
                if($answer->num_rows > 0){
                    foreach($answer as $ans){
                        $ansid = $ans['id'];
                        $ans1 = $ans['answer'];
                    }
                }

                if($answer1 !== 'others' && $ans1 == 'others'){
                    $sub1 = Sub_answer::findByAnswerId($ansid);
                    if($sub1){
                        $sub1->delete();
                    }
                }

                $answer = Answer::findById($ansid);
                // $answer->student_id = $student->id;
                // $answer->section = 2;
                // $answer->answer_number = 1;
                
                if($answer1 !== $ans1){
                    $answer->answer = $answer1;

                    if($answer->update()){
                        if($answer1 == 'others'){
                          // save sub answer also
                          $last = $answer->id;
                          $sub_answer = new Sub_answer();
                          $sub_answer->answer_id = $last;
                          $sub_answer->student_id = $student->id;
                          $sub_answer->answer = trim($sub_answer1);
              
                          if($sub_answer->save()){
                               // saving second answer
                               $answer = Answer::answerByStudentId($student->id, 2, 2);
                               if($answer->num_rows > 0){
                                   foreach($answer as $ans){
                                       $ansid = $ans['id'];
                                       $ans2 = $ans['answer'];
                                   }
                               }
                          
                               $answer = Answer::findById($ansid);

                            // If already answer is not there
                              if($answer2 !== $ans2){
                                  
                                    $answer->answer = $answer2;

                                    if($answer->update()){
                                        redirect("step3.php");
                                    }else{
                                        redirect($_SERVER['PHP_SELF']."?failed2");    
                                    }
                              }else{
                                redirect("step3.php");
                              }

              
                              
                          }else{
                              redirect($_SERVER['PHP_SELF']."?subfailed");
                          }
                        }else{
                             // saving second answer
                             $answer = Answer::answerByStudentId($student->id, 2, 2);
                             if($answer->num_rows > 0){
                                 foreach($answer as $ans){
                                     $ansid = $ans['id'];
                                     $ans2 = $ans['answer'];
                                 }
                             }
                             $answer = Answer::findById($ansid);
                            //  $answer->student_id = $student->id;
                            //  $answer->section = 2;
                            //  $answer->answer_number = 2;
                             
                            if($answer2 !== $ans2){
                                $answer->answer = $answer2;

                                if($answer->update()){
                                    redirect("step3.php");
                                }else{
                                    redirect($_SERVER['PHP_SELF']."?failed2");    
                                }
                            }else{
                                redirect("step3.php");
                            }

                        }
             
                     }else{
                         redirect($_SERVER['PHP_SELF']."?failed1");
                     }
                }else{
                    // saving second answer
                    $answer = Answer::answerByStudentId($student->id, 2, 2);
                    if($answer->num_rows > 0){
                        foreach($answer as $ans){
                            $ansid = $ans['id'];
                            $ans2 = $ans['answer'];
                        }
                    }
                    $answer = Answer::findById($ansid);
                   //  $answer->student_id = $student->id;
                   //  $answer->section = 2;
                   //  $answer->answer_number = 2;
                    
                   if($answer2 !== $ans2){
                       $answer->answer = $answer2;

                       if($answer->update()){
                           redirect("step3.php");
                       }else{
                           redirect($_SERVER['PHP_SELF']."?failed2");    
                       }
                   }else{
                       redirect("step3.php");
                   }
                }
    
        }else{
            if($_POST['answer1'] == 'others'){
                $sub_answer1 = $_POST['answer1sub'];
            }
    
                $answer = new Answer();
                $answer->student_id = $student->id;
                $answer->section = 2;
                $answer->answer_number = 1;
                $answer->answer = $answer1;
    
                if($answer->save()){
                   if($answer1 == 'others'){
                     // save sub answer also
                     $last = $db->last_id();
                     $sub_answer = new Sub_answer();
                     $sub_answer->answer_id = $last;
                     $sub_answer->student_id = $student->id;
                     $sub_answer->answer = trim($sub_answer1);
         
                     if($sub_answer->save()){
                         // saving second answer
                         $answer = new Answer();
                         $answer->student_id = $student->id;
                         $answer->section = 2;
                         $answer->answer_number = 2;
                         $answer->answer = $answer2;
         
                         if($answer->save()){
                             redirect("step3.php");
                         }else{
                             redirect($_SERVER['PHP_SELF']."?failed2");    
                         }
                     }else{
                         redirect($_SERVER['PHP_SELF']."?subfailed");
                     }
                   }else{
                        // saving second answer
                        $answer = new Answer();
                        $answer->student_id = $student->id;
                        $answer->section = 2;
                        $answer->answer_number = 2;
                        $answer->answer = $answer2;
        
                        if($answer->save()){
                            redirect("step3.php");
                        }else{
                            redirect($_SERVER['PHP_SELF']."?failed2");    
                        }
                   }
                }else{
                    redirect($_SERVER['PHP_SELF']."?failed1");
                }
        }
    }
    
   


?>
    <div class="container m-auto">
        <div class="row">
            <div class="col-md-9 col-sm-10 col-xs-12 m-auto">
                <form action="" method="POST">
                <section class="mb-5">

                    <!-- Card -->
                    <div class="card card-cascade narrower">

                    <!-- Card image -->
                    <div class="view view-cascade text-center bg-white">
                        <h2><br> HUDAWI <sup class="text-danger"><strong>+</strong></sup><br></h2>
                        <div class="progress md-progress mt-3">
                            <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <!-- /Card image -->

                    <!-- Card content -->
                    <div class="card-body card-body-cascade text-center table-responsive">

                        <!-- Horizontal Steppers -->
                        <div class="row">
                            <div class="col-md-12">
                                <p class="h5 text-center mb-4 step-head">Step 1</p>

                                <div class="quote">
                                    <blockquote class="blockquote">
                                    “Our goals can only be reached through a vehicle of a plan, in which we must fervently believe, and upon which we must vigorously act. There is no other route to success.”
                                    <footer class="blockquote-footer mb-3 text-right">Pablo Picasso</footer>

                                    </blockquote>
                                </div>

                                <div class="md-form text-left">
                                    <strong>1. What is your passion? </strong><br><span>(The goal you dreamt of it throughout DH life, it may be completely different from your current status. The question is about the ..... you always wanted to be)</span>
                                    <div class="answer1">
                                        <p><?php
                                            if($answered1->num_rows > 0){
                                                echo '<span class="mt-2 d-block">You have selected : <b class="text-primary">'.$answd1.'</b></span>';
                                                if($answd1 == 'others'){
                                                    echo '<p>'.$sub1->answer.'</p>';
                                                }
                                            }
                                        ?></p>
                                        <select id="answer1" class="mdb-select md-form colorful-select dropdown-primary" name="answer1" required searchable="Search here.." required>
                                            <option value="" selected>Pick an answer</option>
                                            <option value="Teacher">Teacher</option>
                                            <option value="Orator">Orator</option>
                                            <option value="Writer">Writer</option>
                                            <option value="Trainer">Trainer</option>
                                            <option value="Civil servant">Civil servant</option>
                                            <option value="Khateeb">Khateeb</option>
                                            <option value="Academician">Academician</option>
                                            <option value="Businessman">Businessman</option>
                                            <option value="Journalist">Journalist</option>
                                            <option value="Job abroad">Job abroad</option>
                                            <option value="IT expert">IT expert</option>
                                            <option value="Government official">Government official</option>
                                            <option value="Social servant">Social servant</option>
                                            <option value="Not planned yet">Not planned yet</option>
                                            <option value="others">Others</option>
                                        </select>
                                        <div id="other">
                                            <input type="text" name="answer1sub" id="answer1sub" class="form-control" placeholder="Let us know what is your passion?">
                                        </div>
                                    </div>
                                </div>

                                <div class="md-form text-left">
                                   <strong>2. Do you think you can make your passion come true?</strong>
                                   <div id="answer2">
                                        <!-- Material inline 1 -->
                                        <div class="form-check form-check-inline">
                                        <input type="radio" class="form-check-input" id="no" name="answer2" value="no" required <?php echo $answd2 == "no" ? 'checked' : ''; ?>>
                                        <label class="form-check-label" for="no">No</label>
                                        </div>

                                        <!-- Material inline 2 -->
                                        <div class="form-check form-check-inline">
                                        <input type="radio" class="form-check-input" id="partially" value="partially" name="answer2" <?php echo $answd2 == "partially" ? 'checked' : ''; ?>>
                                        <label class="form-check-label" for="partially">Partially</label>
                                        </div>

                                        <!-- Material inline 3 -->
                                        <div class="form-check form-check-inline">
                                        <input type="radio" class="form-check-input" id="ofcourse" value="of course" name="answer2"  <?php echo $answd2 == "of course" ? 'checked' : ''; ?>>
                                        <label class="form-check-label" for="ofcourse">Of course</label>
                                        </div>
                                   </div>
                                </div>

                                <div class="text-center mt-4">
                                    <a href="index.php" class="btn btn-primary waves-effect waves-light" name="next">Back</a>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light" name="next">Next</button>
                                </div>
                            </div>
                        </div>
                        <!-- Horizontal Steppers -->

                    </div>
                    <!-- Card content -->

                    </div>
                    <!-- Card -->

                    </section>
                </form>
            </div>
        </div>
    </div>
 

<!-- footer -->
<?php include_once('includes/footer.php'); ?>
<script>
    $(function(){
        $('#other').hide();
        $('#answer1').change(function(){
            var val = $(this).val();

            if(val == 'others'){
                $('#other').fadeIn();
            }else{
                $('#other').fadeOut();
            }
        });
    });
</script>
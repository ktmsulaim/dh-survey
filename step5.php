<?php include_once('includes/header.php'); ?>
<?php if($stdsession->isVerified()){}else{ redirect("login.php"); } ?>
<?php



    $student_id = Student::studentByEnroll($_SESSION['enroll_no']);
    $student = Student::findById($student_id);


    if(Student::checkAll($student->id) > 0){
        redirect("finish.php");
    }

    // $answers = Answer::getBySection($student_id, $section);

    if(isset($_POST['next'])){
        $rating = $_POST['rating'];
        $suggestions = trim($_POST['suggestions']);

        if(empty($rating)){
            redirect($_SERVER['PHP_SELF']."?rating");
        }else{
            if(empty($suggestions)){
                redirect($_SERVER['PHP_SELF']."?feedback");
            }else{
                if(Student::hasStep5($student->id)){
                    redirect($_SERVER['PHP_SELF']."?already");
                }else{
                    $feedback = new Feedback();
                    $feedback->student_id = $student->id;
                    $feedback->rating = $rating;
                    $feedback->feedback = trim($suggestions);

                    if($feedback->save()){
                        redirect("finish.php");
                    }else{
                        redirect($_SERVER['PHP_SELF']."?failed");
                    }
                }
            }
        }
    }

    $msg = '';

    if(isset($_GET['rating'])){
        $msg = '<div class="note note-danger">Please rate the P.G system</div>';
    }elseif(isset($_GET['feedback'])){
        $msg = '<div class="note note-danger">Please write any suggestion to improve the P.G system</div>';
    }

?>
    <div class="container m-auto">
        <div class="row">
            <div class="col-md-9 col-sm-10 col-xs-12 m-auto">
                <form action="" method="POST">
                <section class="mb-5">

                    <!-- Card -->
                    <div class="card card-cascade narrower">

                    <!-- Card image -->
                    <div class="view view-cascade text-center bg-white">
                        <h2><br> HUDAWI <sup class="text-danger"><strong>+</strong></sup><br></h2>
                        <div class="progress md-progress mt-3">
                            <div class="progress-bar" role="progressbar" style="width: 90%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <!-- /Card image -->

                    <!-- Card content -->
                    <div class="card-body card-body-cascade text-center table-responsive">

                        <!-- Horizontal Steppers -->
                        <div class="row">
                            <div class="col-md-12">
                                <p class="h5 text-center mb-4 step-head">Step 4</p>
                                <?php echo $msg; ?>
                                <div class="md-form text-left">
                                    <strong>What problems and defects you see in DH PG system? </strong><br><span>Kindly suggest the solutions for problems you note<span>
                                </div>
                                <div class="md-form">
                                    <div class="container">
                                        <div id="rateMe" class="rating-faces"></div>
                                    </div>
                                </div>
                                <div class="md-form">
                                    <textarea id="form103" class="md-textarea form-control" name="suggestions" rows="5" required></textarea>
                                    <label for="form103">Your suggestions</label>
                                </div>

                                <div class="text-center mt-4">
                                    <a href="step4.php" class="btn btn-primary waves-effect waves-light">Back</a>
                                    <button type="submit" class="btn btn-danger waves-effect waves-light" name="next">Submit</button>
                                </div>
                            </div>
                        </div>
                        <!-- Horizontal Steppers -->

                    </div>
                    <!-- Card content -->

                    </div>
                    <!-- Card -->

                    </section>
                </form>
            </div>
        </div>
    </div>
 

<!-- footer -->
<?php include_once('includes/footer.php'); ?>
<script>
$(function(){
    $("#rateMe").emotionsRating({
    bgEmotion: "happy",

    emotions: ['angry','disappointed','meh', 'happy', 'inLove'],

    count: 5,

    // #color of emoji
    // #gold, red, blue, green, black,
    // #brown, pink, purple, orange
    color: "blue",

    // #size of emoji
    emotionSize: 30,

    // #input name
    inputName: "rating",

    // #initialize the rating number
    initialRating: 3,

    // #initialize the rating status
    initialRating: true

    });
});
</script>

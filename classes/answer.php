<?php

class Answer extends Db_object {
    public $id;
    public $student_id;
    public $section;
    public $answer_number;
    public $answer;

    protected static $db_table = "answers";
    protected static $db_table_fields = array('student_id', 'section', 'answer_number', 'answer');

    public static function checkAlready($section, $answer_number){
        global $db;
        $result = $db->query("SELECT * FROM answers WHERE section = {$section} AND answer_number = {$answer_number}");
        if($result->num_rows > 0){
            foreach($result as $answer){
                $id = $answer['id'];
            }
            return $id;
        }else{
            return false;
        }
    }

    public static function getBySection($student_id, $section){
        global $db;
        $result = $db->query("SELECT * FROM answers WHERE student_id = {$student_id} AND section = {$section}");
        return $result;
    }

    public static function answerByStudentId($student_id, $section, $answer_number){
        global $db;
        $result = $db->query("SELECT * FROM answers WHERE student_id = {$student_id} AND section = {$section} AND answer_number = {$answer_number}");
        return $result;
    }


} // End of class


?>
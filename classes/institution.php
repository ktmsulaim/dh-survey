<?php

class Institution extends Db_object {
    public $id; 
    public $name;

    protected static $db_table = "institutions";
    protected static $db_table_fields = array('name');
}

?>
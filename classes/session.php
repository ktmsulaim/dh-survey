<?php


class Session {
    private $loggedIn = false;
    public $userId;
    
    
    function __construct(){
        session_name('main');
        session_start();
        $this->authCheck();
    }
    
    private function authCheck(){
        if(isset($_SESSION['user_id'])){
            $this->userId = $_SESSION['user_id'];
            $this->loggedIn = true;
        }else{
            unset($_SESSION['user_id']);
            $this->loggedIn = false;
        }
    }
    
    public function isLoggedIn(){
        return $this->loggedIn;
    }
    
    public function logIn($user){
        if($user){
            $this->userId = $_SESSION['user_id'] = $user->id;
            $this->loggedIn = true;
        }
    }
    
    public function logOut(){
        unset($_SESSION['user_id']);
        unset($this->userId);
        $this->loggedIn = false;
    }
}

$session = new Session();


?>
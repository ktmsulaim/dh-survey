<?php


class Student_session {
    private $verify = false;
    public $enroll_no;
    private $confirm = false;
    
    
    function __construct(){
        $this->authCheck();
    }
    
    private function authCheck(){
        if(isset($_SESSION['enroll_no'])){
            $this->enroll_no = $_SESSION['enroll_no'];
            $this->verify = true;
        }else{
            unset($_SESSION['enroll_no']);
            $this->verify = false;
        }
    }
    
    public function isVerified(){
        return $this->verify;
    }
    
    public function verify($student){
        if($student){
            $this->enroll_no = $_SESSION['enroll_no'] = $student->enroll_no;
            $this->verify = true;
        }
    }

    public function confirm(){
        if(isset($_SESSION['confirm'])){
            $this->confirm = true;
        }else{
            unset($_SESSION['confirm']);
            $this->confirm = false;
        }
        
    }

    public function confirmed(){
        if($this->confirm == true){
            return true;
        }else{
            return false;
        }
    }
    
    public function getOut(){
        unset($_SESSION['enroll_no']);
        unset($this->enroll_no);
        $this->verify = false;
    }
}

$stdsession = new Student_session();


?>
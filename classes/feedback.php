<?php

class Feedback extends Db_object {
    public $id;
    public $student_id;
    public $rating;
    public $feedback;

    protected static $db_table = "feedback";
    protected static $db_table_fields = array('student_id', 'rating', 'feedback');

    public static function getByStudentId($student_id){
        return self::query("SELECT * FROM feedback WHERE student_id = {$student_id}");
    }

    public static function avg()
    {
        $result = self::query("SELECT AVG(rating) as rating FROM ".self::$db_table);
        return !empty($result) ? array_shift($result) : false;
    }

    public static function getByRating($rating)
    {
        $students = self::query("SELECT student_id FROM ".self::$db_table." WHERE rating = {$rating}");
        return count($students);
    }
}

?>
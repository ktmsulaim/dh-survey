<?php

class Sub_answer extends Db_object {
    public $id;
    public $student_id;
    public $answer_id;
    public $answer;

    protected static $db_table = "sub_answers";
    protected static $db_table_fields = array('student_id', 'answer_id', 'answer');

    public static function findByAnswerId($answer_id){
        $result = self::query("SELECT * FROM sub_answers WHERE answer_id = {$answer_id}");
        return !empty($result) ? array_shift($result) : false;
    }
    public static function getByAnswerId($answer_id){
        $result = self::query("SELECT * FROM sub_answers WHERE answer_id = {$answer_id}");
        return !empty($result) ? $result : false;
    }



} // End of Class


?>
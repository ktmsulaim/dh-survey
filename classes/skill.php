<?php

class Skill extends Db_object {
    public $id;
    public $student_id;
    public $skill;

    protected static $db_table = "skills";
    protected static $db_table_fields = array('student_id', 'skill');

    public static function findByStudentId($student_id)
    {
        return self::query("SELECT * FROM skills WHERE student_id = {$student_id}");
    }

    public static function getBySkill($skill)
    {
        return self::query("SELECT student_id FROM ".self::$db_table." WHERE skill = '{$skill}'");
    }
} // End of the class

?>
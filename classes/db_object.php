<?php

class Db_object {
    
        public static function query($sql){
            global $db;
            
            $results = $db->query($sql);
            $objectArray = array();
            
            while($row = mysqli_fetch_array($results)){
                $objectArray[] = static::instance($row);
            }
            return $objectArray;
        }
    
    
        public static function all(){
            
            global $db;
            
            return static::query("SELECT * FROM ". static::$db_table);
        }
    
    
        public static function findById($id){
            global $db;
            
            $user = static::query("SELECT * FROM ".static::$db_table." WHERE id=".$id." LIMIT 1");
            return !empty($user) ? array_shift($user) : false;
        }
    
        public static function instance($user){
            $calling_class = get_called_class();
            $obj = new $calling_class;
            
            foreach($user as $userColumn => $value){
                if($obj->hasColumn($userColumn)){
                    $obj->$userColumn = $value;
                }
            }
            
            return $obj;
        }
    
    
        private function hasColumn($userColumn){
            $object = get_object_vars($this);
            
            return array_key_exists($userColumn, $object);
        }
    
        private function properties(){
            
            $properties = array();
            
            foreach(static::$db_table_fields as $db_field){
                if(property_exists($this, $db_field)){
                    $properties[$db_field] = $this->$db_field;
                }
            }
            
            return $properties;
        }
    
        protected function cleanProps(){
            global $db;
            
            $clean_props = array();
            
            foreach($this->properties() as $key => $value){
                $clean_props[$key] = $db->escape_string($value);
            }
            
            return $clean_props;
        }
    
    
        public function save(){
            return isset($this->id) ? $this->update() : $this->create();
        }
        
    
        public function create(){
            global $db;
            
            $properties = $this->cleanProps();
            
            
            $sql = "INSERT INTO ".static::$db_table." ( ".implode(",", array_keys($properties))." ) ";
            $sql .= "VALUES ('".implode("','", array_values($properties))."')";
            
            if($db->query($sql)){
                $this->id = $db->last_id();
                return true;
            }else{
                return false;
            }
        }
    
        public function update(){
            global $db;
            
            $properties = $this->cleanProps();
            $prop_pair = array();
            
            foreach($properties as $key => $value){
                $prop_pair[] = "{$key}='{$value}'";
            }
            
            
            $sql = "UPDATE ".static::$db_table." SET ";
            $sql .= implode(", ", $prop_pair);
            $sql .= " WHERE id = {$this->id}";
            
            $db->query($sql);
            
            if(mysqli_affected_rows($db->connection) == 1){
                return true;
            }else{
                return false;
            }
            
        }
    
        public function delete(){
            global $db;
            
            $sql = "DELETE FROM ".static::$db_table." WHERE id=" . $db->escape_string($this->id);
            $sql .= " LIMIT 1";
            
            $db->query($sql);
            
            return mysqli_affected_rows($db->connection) == 1 ? true : false;
        }

        public static function getCount($condition){
            global $db;
            $result = $db->query("SELECT COUNT(*) as total FROM ".static::$db_table." ".$condition);
            
            if($result){
                foreach($result as $count){
                    $total = $count['total'];
                }
                return $total;
            }
        }
    
} // End of class

?>
<div class="row">
    <div class="col-md-6">
        <h4>Add new photo</h4>
        <form action="" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="desc">Description</label>
                <textarea name="desc" id="desc" cols="30" rows="10" style="resize:none" class="form-control" required></textarea>
            </div>
            <div class="form-group">
                <label for="img">Image</label>
                <input type="file" name="img" id="img" class="form-control-file" required>
            </div>
            <input type="submit" name="upload" class="btn btn-primary" value="Upload">
        </form>
    </div>
</div>
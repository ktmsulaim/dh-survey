<?php
    
class User extends Db_object{
    
        public $id;
        public $username;
        public $password;
        public $first_name;
        public $last_name;
        public $profile;
        public $directory = "images/users";
        public $dummy = "user.svg";
        
        
        protected static $db_table = "users";
        protected static $db_table_fields = array('username', 'password', 'first_name', 'last_name', 'profile');
    
       
        public function user_image(){
            return empty($this->profile) ? $this->directory.DS.$this->dummy : $this->directory.DS.$this->profile;
        }
    
        public static function verifyUser($username, $password){
            global $db;
            
            $username = $db->escape_string($username);
            $password = $db->escape_string($password);
            
            $sql = "SELECT * FROM ".self::$db_table." WHERE username = '{$username}' AND password = '{$password}' LIMIT 1";
            
            $user = self::query($sql);
            
            return !empty($user) ? array_shift($user) : false;
        }
    
    // passing $_FILES['uploaded_file'] into arguement
    
        public function set_file($file){
            if(empty($file) || !$file || !is_array($file)){
                $this->custom_errors[] = "There was no file to upload";
                return false;
            }elseif($file['error'] !=0){
                $this->custom_errors[] = $this->upload_errors[$file['error']];
            }else{
                $this->profile = basename($file['name']);
                $this->tmp_name = $file['tmp_name'];
                $this->type = $file['type'];
                $this->size = $file['size'];
            }
        }
    
    
    //save method dirction to create() / update()
    
    public function upload_photo(){
            
            //validation
            if(!empty($this->custom_errors)){
                return false;
            }
            
            if(empty($this->profile) || empty($this->tmp_name)){
                $this->custom_errors[] = "The file not available";
                return false;
            }
            
            $target_path = SITE_ROOT . DS . 'admin' . DS . $this->directory . DS . $this->profile;
            
            if(file_exists($target_path)){
                $this->custom_errors[] = "The file {$target_path} is already existing";
                return false;
            }
            
            if(move_uploaded_file($this->tmp_name, $target_path)){
                unset($this->tmp_name);
                return true;
            }
    }
    
   public function picture_path(){
        return $this->directory.DS.$this->profile;
    }
    
    public function deletePhoto(){
        if($this->delete()){
            $target_path = SITE_ROOT.DS.'admin'.DS.$this->picture_path();
            return unlink($target_path) ? true : false;
        }else{
            return false;
        }
    }
    
    
        
    
        
    
    
} // end of User class
    
?>
<?php

class Photo extends Db_object {
    
        protected static $db_table = "photos";
        protected static $db_table_fields = array('title','caption', 'description', 'alt', 'filename', 'type', 'size');
        
        public $id;
        public $title;
        public $caption;
        public $description;
        public $alt;
        public $filename;
        public $type;
        public $size;
        public $created_at;
        public $updated_at;
    
        public $tmp_name;
        public $directory = "images";
        public $custom_errors = array();
        public $upload_errors = array(
            UPLOAD_ERR_OK => 'There is no error, the file uploaded with success.',
            UPLOAD_ERR_INI_SIZE => 'The uploaded file exceeds the upload_max_filesize directive in php.ini.',
            UPLOAD_ERR_FORM_SIZE => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.',
            UPLOAD_ERR_PARTIAL => 'The uploaded file was only partially uploaded.',
            UPLOAD_ERR_NO_FILE => 'No file was uploaded.',
            UPLOAD_ERR_NO_TMP_DIR => 'Missing a temporary folder.',
            UPLOAD_ERR_CANT_WRITE => 'Cannot write to target directory. Please fix CHMOD.',
            UPLOAD_ERR_EXTENSION => 'A PHP extension stopped the file upload.'
        );
    
        // passing $_FILES['uploaded_file'] into arguement
    
        public function set_file($file){
            if(empty($file) || !$file || !is_array($file)){
                $this->custom_errors[] = "There was no file to upload";
                return false;
            }elseif($file['error'] !=0){
                $this->custom_errors[] = $this->upload_errors[$file['error']];
            }else{
                $this->filename = basename($file['name']);
                $this->tmp_name = $file['tmp_name'];
                $this->type = $file['type'];
                $this->size = $file['size'];
            }
        }
    
    
    //save method dirction to create() / update()
    
    public function save(){
        if($this->id){
            $this->update();
        }else{
            
            //validation
            if(!empty($this->custom_errors)){
                return false;
            }
            
            if(empty($this->filename) || empty($this->tmp_name)){
                $this->custom_errors[] = "The file not available";
                return false;
            }
            
            $target_path = SITE_ROOT . DS . 'admin' . DS . $this->directory . DS . $this->filename;
            
            if(file_exists($target_path)){
                $this->custom_errors[] = "The file {$target_path} is already existing";
                return false;
            }
            
            if(move_uploaded_file($this->tmp_name, $target_path)){
                if($this->create()){
                    unset($this->tmp_name);
                    return true;
                }else{
                    $this->custom_errors[] = "The file directory probably does not have permission to uplaod files";
                    return false;
                }
            }
        }
    }
    
    public function picture_path(){
        return $this->directory.DS.$this->filename;
    }
    
    public function deletePhoto(){
        if($this->delete()){
            $target_path = SITE_ROOT.DS.'admin'.DS.$this->picture_path();
            return unlink($target_path) ? true : false;
        }else{
            return false;
        }
    }
    
    
    
}  // End of the class

?>


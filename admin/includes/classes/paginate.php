<?php

class Paginate {
    public $page;
    public $per_page;
    public $total;


    public function __construct($page=1, $per_page=4, $total=0)
    {
        $this->page = $page;
        $this->per_page = $per_page;
        $this->total = $total;
    }

    public function next(){
        return $this->page + 1;
    }

    public function prev(){
        return $this->page - 1;
    }

    public function total_page(){
        return ceil($this->total / $this->per_page);
    }

    public function hasNext(){
        return $this->next() <= $this->total_page() ? true :  false;
    }

    public function hasPrev(){
        return $this->prev() >= 1 ? true : false;
    }

    public function offset(){
        return ($this->page - 1) * $this->per_page;
    }


}

?>
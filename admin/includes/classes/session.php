<?php


class Session {
    private $loggedIn = false;
    public $userId;
    public $count = 0;
    
    
    function __construct(){
        session_start();
        $this->view();
        $this->authCheck();
    }
    
    private function authCheck(){
        if(isset($_SESSION['user_id'])){
            $this->userId = $_SESSION['user_id'];
            $this->loggedIn = true;
        }else{
            unset($_SESSION['user_id']);
            $this->loggedIn = false;
        }
    }
    
    public function isLoggedIn(){
        return $this->loggedIn;
    }
    
    public function logIn($user){
        if($user){
            $this->userId = $_SESSION['user_id'] = $user->id;
            $this->loggedIn = true;
        }
    }
    
    public function logOut(){
        unset($_SESSION['user_id']);
        unset($this->userId);
        $this->loggedIn = false;
    }
    
    public function view(){
        if(isset($_SESSION['count'])){
            return $this->count = $_SESSION['count']++;
        }else{
            return $_SESSION['count'] = 1;
        }
    }
}

$session = new Session();


?>
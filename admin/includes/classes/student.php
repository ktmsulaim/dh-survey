<?php
    class Student extends Db_object {
        public $id;
        public $department_id;
        public $name;
        public $enroll_no;
        public $exam_no;
        public $status;
        public $dep;

        public $directory = "images/students";
        public $dummy = "student.svg";


        protected static $db_table = "students";
        protected static $db_table_fields = array('department_id', 'name', 'enroll_no', 'exam_no');


        public function student_image(){
            if(!empty($this->profile)){
                if(file_exists($this->directory.DS.$this->profile)){
                    return $this->directory.DS.$this->profile;
                }else{
                    return $this->directory.DS.$this->dummy;
                }
            }else{
                return $this->directory.DS.$this->dummy;
            }
            
        }

        public static function verifyStudent($enroll_no, $exam_no){
            global $db;
            
            $enroll_no = $db->escape_string($enroll_no);
            $exam_no = $db->escape_string($exam_no);
            
            $sql = "SELECT * FROM ".self::$db_table." WHERE enroll_no = '{$enroll_no}' AND exam_no = '{$exam_no}' LIMIT 1";
            
            $student = self::query($sql);
            
            return !empty($student) ? array_shift($student) : false;
        }

        public static function studentByEnroll($enroll_no){
            global $db;

            $result = $db->query("SELECT * FROM students WHERE enroll_no = '{$enroll_no}' LIMIT 1");

            if($result->num_rows >= 1){
                foreach($result as $student){
                    $id = $student['id'];
                }
                return $id;
            }
        }

        public static function setInst($student_id, $institution_id){
            global $db;
            $result = $db->query("INSERT INTO set_inst(student_id, institution_id) VALUES ({$student_id}, {$institution_id})");
            if($result){
                return true;
            }else{
                return false;
            }
        }

        public static function updateInst($student_id, $institution_id){
            global $db;
            $update = $db->query("UPDATE set_inst SET institution_id = {$institution_id} WHERE student_id = {$student_id}");
            
            if($update){
                return true;
            }else{
                return false;
            }
        }

        public static function hasStep1($student_id){
            global $db;
            $result = $db->query("SELECT institution_id FROM set_inst WHERE student_id = {$student_id}");
            if($result->num_rows > 0){
                return true;
            }else{
                return false;
            }
        }

        public static function hasStep2($student_id){
            global $db;
            $result = $db->query("SELECT * FROM answers WHERE section = 2 AND student_id = {$student_id}");
            if($result->num_rows >= 2){
                return true;
            }else{
                return false;
            }
        }

        public static function hasStep3($student_id){
            global $db;
            $result = $db->query("SELECT id FROM answers WHERE section = 3 AND student_id = {$student_id}");
            if($result->num_rows >= 3){
                return true;
            }else{
                return false;
            }
        }

        public static function hasStep4($student_id){
            global $db;
            $result = $db->query("SELECT id FROM skills WHERE student_id = {$student_id}");
            if($result->num_rows > 0){
                return true;
            }else{
                return false;
            }
        }

        public static function hasStep5($student_id){
            global $db;
            $result = $db->query("SELECT id FROM feedback WHERE student_id = {$student_id}");
            if($result->num_rows > 0){
                return true;
            }else{
                return false;
            }
        }

        public static function setAll($student_id){
            global $db;
            if(self::hasStep1($student_id) && self::hasStep2($student_id) && self::hasStep3($student_id) && self::hasStep4($student_id) && self::hasStep5($student_id)){
               
                    if(self::checkAll($student_id) > 0){
                        
                    }else{
                        $insert = $db->query("INSERT INTO registered(student_id) VALUES({$student_id})");
                        if($insert){
                            return true;
                        }else{
                            return false;
                        }
                    }
                }
            }
               

        public static function checkAll($student_id){
            global $db;
            $check = $db->query("SELECT COUNT(id) as total FROM registered WHERE student_id = {$student_id}");
            if($check){
                foreach($check as $checked){
                    $total = $checked['total'];
                }
                
                return $total;
            }else{
                return false;
            }
        }

        public static function getInst($student_id){
            global $db;
            $result = $db->query("SELECT institution_id FROM set_inst WHERE student_id = {$student_id}");
            foreach($result as $id){
                $institution_id = $id['institution_id'];
            }

            return $institution_id;
        }


        public static function studentsByInstitute($institute_id)
        {
            return self::query("SELECT students.* FROM students LEFT JOIN set_inst ON students.id = set_inst.student_id LEFT JOIN institutions ON set_inst.institution_id = institutions.id WHERE institutions.id = {$institute_id}");
        }

        public static function studentsBySection($section, $answer_num, $answer)
        {
            return self::query("SELECT students.* FROM students LEFT JOIN answers ON answers.student_id = students.id WHERE answers.section = {$section} AND answers.answer_number = {$answer_num} AND answers.answer = '{$answer}'");
        }

        public static function sectionone($answer1, $answer2)
        {
            return self::query("SELECT students.* FROM students LEFT JOIN answers ON students.id = answers.student_id WHERE answers.section = 2 AND answers.answer_number = 1 AND answers.answer = '{$answer1}' AND answers.student_id IN (SELECT student_id FROM answers WHERE answer_number = 2 AND answer = '{$answer2}')");
        }

        public static function sectiontwo($answer_number, $answer1)
        {
            return self::query("SELECT students.* FROM students LEFT JOIN answers ON students.id = answers.student_id WHERE answers.section = 3 AND answers.answer_number = {$answer_number} AND answers.answer = '{$answer1}'");
        }

        public static function sectionTwoConditional($answer, $subanswer)
        {
            return self::query("SELECT students.* FROM students LEFT JOIN answers ON students.id = answers.student_id LEFT JOIN sub_answers ON answers.id = sub_answers.answer_id WHERE answers.section = 3 AND answers.answer_number = 1 AND answers.answer = '{$answer}' AND sub_answers.answer = '{$subanswer}'");
        }

        public static function studentsBySkill($skill)
        {
            return self::query("SELECT students.* FROM ".self::$db_table." LEFT JOIN skills ON students.id = skills.student_id WHERE skills.skill = '{$skill}'");
        }

        public static function studentsWithFeedback()
        {
            return self::query("SELECT students.*, feedback.feedback, feedback.rating FROM ".self::$db_table." LEFT JOIN feedback ON students.id = feedback.student_id WHERE feedback.feedback IS NOT NULL");
        }

        public static function getRegistered()
        {
            global $db;
            $check = $db->query("SELECT COUNT(*) as total FROM registered");
            if($check){
                foreach($check as $checked){
                    $total = $checked['total'];
                }
                
                return $total;
            }else{
                return false;
            }
        }

        public function getDep($id)
        {
            $department = "";

            if ((int)$id === 1) {
                $department = 'Quran';
            } elseif((int)$id === 2) {
                $department = 'Hadith';
            } elseif((int)$id === 3) {
                $department = 'Fiqh';
            } elseif((int)$id === 4) {
                $department = 'Aqeeda';
            } elseif((int)$id === 5) {
                $department = 'Da\'wa';
            } elseif((int)$id === 6) {
                $department = 'Arabic language';
            } else {
                $department ="NULL";
            }
            return $department;
        }

    } //End of class
?>
<?php
    
class Comment extends Db_object{
    
        public $id;
        public $photo_id;
        public $author;
        public $body;
        public $date;
        
        
        protected static $db_table = "comments";
        protected static $db_table_fields = array('photo_id', 'author', 'body', 'date');
    

        public static function create_comment($photo_id, $author, $body){
            global $db;

            // Validation
            if(!empty($photo_id) && !empty($author) && !empty($body)){
                $comment= new Comment();
                $comment->photo_id = (int)$photo_id;
                $comment->author = $db->escape_string($author);
                $comment->body = $db->escape_string($body);

                return $comment;
            }else{
                return false;
            }
        }


        public static function findComments($photo_id){
            global $db;

            $sql = "SELECT * FROM ".self::$db_table;
            $sql .= " WHERE photo_id = ".$db->escape_string($photo_id);
            $sql .= " ORDER BY photo_id ASC";

            return self::query($sql);
        }
    
        
    
    
} // end of User class
    
?>
<?php

class Post{
    
    public $id;
    public $user_id;
    public $title;
    public $body;
    
    
    public static function query($sql){
        global $db;
        
        $result = $db->query($sql);
        $sqlArray = array();
        
        while($row = mysqli_fetch_array($result)){
            $sqlArray[] = self::instance($row);
        }
        return $sqlArray;
    }
    
    public static function allPosts(){
        global $db;
        return self::query("SELECT * FROM posts");
    }
    
    public static function findById($id){
        global $db;
        $post = self::query("SELECT * FROM posts WHERE id=".$id." LIMIT 1");
       
        return $post;
    }
    
    public static function instance($post){
        $obj = new self;
        foreach($post as $postColumn=>$value){
            if($obj->hasColumn($postColumn)){
                $obj->$postColumn = $value;
            }
        }
        
        return $obj;
    }
    
    private function hasColumn($postColumn){
        $object = get_object_vars($this);
        return array_key_exists($postColumn, $object);
    }
    
    
    
}


?>
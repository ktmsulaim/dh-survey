  </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ckeditor.js"></script>
    <script src="js/datatables.min.js"></script>
    <script src="js/init.js"></script>

    <script>
      $(function(){
        $('.dataTable').dataTable();
      });
    </script>

</body>

</html>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
           <h4><?php echo isset($_GET['edit']) ? 'Edit user' : 'Add user'; ?></h4>
            <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
             <div class="form-group">
                <label for="first_name">First name:</label>
                <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo isset($_GET['edit']) ? $first_name : ''; ?>" required>
              </div>
              <div class="form-group">
                <label for="last_name">Last Name:</label>
                <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo isset($_GET['edit']) ? $last_name : ''; ?>" required>
              </div>
              <div class="form-group">
                <label for="username">Username:</label>
                <input type="username" class="form-control" id="username" name="username" value="<?php echo isset($_GET['edit']) ? $username : ''; ?>" required>
              </div>
              <div class="form-group">
                <label for="pwd">Password:</label>
                <input type="<?php echo isset($_GET['edit']) ? 'text' : 'password'; ?>" class="form-control" id="pwd" name="password" value="<?php echo isset($_GET['edit']) ? $password : ''; ?>" required>
              </div>
              <?php
                if(isset($_GET['edit']) && isset($_GET['id'])){
                    echo '<button type="submit" class="btn btn-primary" name="update">Update</button><a href="users.php" class="btn btn-default" style="margin-left:5px">Cancel</a>';
                }else{
                    echo '<button type="submit" class="btn btn-default" name="submit">Add</button>';
                }
                
                ?>
            </form>
        </div>
        <div class="col-md-6">
           <h4>All users</h4>
            <table class="table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Username</th>
                        <th colspan="2">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $users = User::all();
                    
                        foreach($users as $user){
                            echo '<tr>
                                    <td>'.$user->id.'</td>
                                    <td>'.$user->first_name.'</td>
                                    <td>'.$user->last_name.'</td>
                                    <td>'.$user->username.'</td>
                                    <td><a href="users.php?edit&id='.$user->id.'" title="Edit"><i class="fa fa-edit"></i></a>
                                    </td>
                                    <td><a style="color:red" href="users.php?delete&id='.$user->id.'" title="Delete"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>';
                        }
                    
                    ?>
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
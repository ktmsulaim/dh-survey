<?php

require_once('config.php');

class Database{
    public $connection;
    
    // Automatic connection
    
    function __construct(){
        $this->connect();
    }
    
    public function connect(){
        $this->connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        
        if($this->connection->connect_errno){
            die("Connection was failed. ". $this->connection->connect_error);
        }
    }
    
    // querying results
    
    public function query($sql){
        $result = $this->connection->query($sql);
        
        $this->confirm_query($result);
        
        return $result;
    }
    
    private function confirm_query($result){
        if(!$result){
            die('Query failed.' . $this->connection->error);
        }
    }
    
    public function escape_string($string){
        $escaped_string = $this->connection->real_escape_string($string);
        return $escaped_string;
    }
    
    public function last_id(){
        return mysqli_insert_id($this->connection);
    }

    public function timestamp(){
        date_default_timezone_set('Asia/Kolkata');
        $date = date('Y-m-d H:i:s');

        return $date;
    }
}

$db = new Database();


?>
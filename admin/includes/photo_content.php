

<div class="row">
    <div class="col-md-12">
        <?php echo $msg; ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h4>All photos</h4>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Image</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Size</th>
                    <th>Comments</th>
                </tr>
            </thead>
            <tbody>
               <?php foreach($photos as $photo) : ?>
                    <tr>
                        <td><?php echo $photo->id; ?></td>
                        <td><img src="<?php echo $photo->filename !== '' ? $photo->picture_path() : 'http://placehold.it/62x62'; ?>" alt="<?php echo $photo->title; ?>" width="150">
                            <div class="pic_tools" style="margin-top:10px">
                                <a href="delete_photo.php?id=<?php echo $photo->id; ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash fa-fw"></i> Delete</a>
                                <a href="edit_photo.php?id=<?php echo $photo->id; ?>" class="btn btn-info btn-sm"><i class="fa fa-edit fa-fw"></i> Edit</a>
                                <a href="../photo.php?id=<?php echo $photo->id; ?>" class="btn btn-primary btn-sm"><i class="fa fa-eye fa-fw"></i> View</a>
                            </div>
                        </td>
                        <td><?php echo $photo->title; ?></td>
                        <td><?php echo $photo->description; ?></td>
                        <td><?php echo $photo->size; ?></td>
                        <td>
                            <?php 
                                $comments = Comment::findComments($photo->id);
                                echo '<a href="comments.php?id='.$photo->id.'"><span class="badge badge-primary">'.count($comments).'</span></a>';
                            ?>
                        </td>
                    </tr>
                  <?php  endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="clearfix"></div>
</div>
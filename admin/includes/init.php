<?php

defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);

define('SITE_ROOT', 'C:' . DS . 'laragon' . DS . 'www' . DS . 'oop' . DS . 'gallery');

define('INCLUDES_PATH', SITE_ROOT . DS . 'admin' . DS . 'includes');

define('CLASSES_PATH', INCLUDES_PATH . DS . 'classes');

require_once('functions.php');    
require_once('config.php');
require_once('database.php');
require_once('classes/db_object.php');
require_once('classes/user.php');
require_once('classes/photo.php');
require_once('classes/comment.php');
require_once('classes/post.php');
require_once('classes/session.php');
require_once('classes/paginate.php');

?>

<?php include("includes/header.php"); ?>
<?php

    $response = "";

    if(isset($_POST['submit'])){
        $user = new User();
        $user->username = $_POST['username'];
        $user->password = $_POST['password'];
        $user->first_name = $_POST['first_name'];
        $user->last_name = $_POST['last_name'];

        if($user->create()){
            $response = '<div class="alert alert-success"><b>Success!</b> The user has been added!</div>';
        }else{
            $response = "";
        }
    }


    if(isset($_GET['edit']) && isset($_GET['id'])){
        $id = $_GET['id'];
        $_SESSION['user_id'] = $id;

        $user = User::findById($id);

        $first_name = $user->first_name;
        $last_name = $user->last_name;
        $username = $user->username;
        $password = $user->password;
    
    }


    if(isset($_POST['update'])){
        $id = $_SESSION['user_id'];
        
        $user = User::findById($id);
        
        $user->id = $id;
        $user->username = $_POST['username'];
        $user->password = $_POST['password'];
        $user->first_name = $_POST['first_name'];
        $user->last_name = $_POST['last_name'];

        if($user->update()){
            $response = '<div class="alert alert-success"><b>Success!</b> The user has been updated!</div>';
            unset($_SESSION['user_id']); 
        }else{
            $response = "";
        }
    }

    if(isset($_GET['delete']) && isset($_GET['id'])){
        $id = $_GET['id'];
        
        $user = User::findById($id);
        
        if($user->delete()){
             $response = '<div class="alert alert-danger"><b>Success!</b> The user has been deleted!</div>';
        }else{
            $response = "";
        }
        
    }



?>
       
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
                <?php include('includes/top_nav.php'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <?php include("includes/side_nav.php"); ?>
            <!-- /.navbar-collapse -->
        </nav>
               
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                <h1 class="page-header">
                    Users
                    <small>Beta</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-users"></i> <a href="users.php">Users</a>
                    </li>
                </ol>
            </div>
            </div>

            <div class="container-fluid">
                <!-- Page Heading -->
                <?php echo $response; include("includes/user_content.php"); ?>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

  <?php include("includes/footer.php"); ?>
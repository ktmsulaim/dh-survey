<?php include("includes/init.php"); ?>
<?php if($session->isLoggedIn()){}else{ redirect("login.php"); } ?>

<?php

if(empty($_GET['id'])){
    redirect("users.php");
}

$student = Student::findById($_GET['id']);

if($student){
    if($student->delete()){
        redirect("users.php?success");
    }else{
        redirect("users.php?failed");
    }
    
}else{
    redirect("users.php");
}

?>
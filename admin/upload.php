<?php include("includes/header.php"); ?>
<?php if($session->isLoggedIn()){}else{ redirect("login.php"); } ?>
<?php
    $msg = "";
    if(isset($_POST['upload'])){
        $photo = new Photo();
        $photo->title = $_POST['title'];
        $photo->description = $_POST['desc'];
        $photo->set_file($_FILES['img']);
        
        if($photo->save()){
            $msg = "Photo uploaded successfully";
        }else{
            $msg = join('<br>', $photo->custom_errors);
        }
    }

?>

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
                <?php include('includes/top_nav.php'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <?php include("includes/side_nav.php"); ?>
            <!-- /.navbar-collapse -->
        </nav>
               
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                <h1 class="page-header">
                    Upload
                    <small>Beta</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-upload"></i> <a href="upload.php">Upload</a>
                    </li>
                </ol>
            </div>
            </div>

            <div class="container-fluid">
                <?php echo $msg; ?>
                <!-- Page Heading -->
                <?php include("includes/upload_content.php"); ?>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

  <?php include("includes/footer.php"); ?>
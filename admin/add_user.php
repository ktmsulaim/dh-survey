<?php include("includes/header.php"); ?>
<?php if($session->isLoggedIn()){}else{ redirect("login.php"); } ?>

<?php

$student = new Student();

if(isset($_POST['create'])){
    $student->name = $_POST['name'];
    $student->enroll_no = $_POST['enroll_no'];
    $student->exam_no = $_POST['exam_no'];
    $student->department_id = $_POST['department_id'];

    
    if($student->save()){
        redirect("users.php?added");
    }else{
        redirect($_SERVER['PHP_SELF']."?failed");
    }
}

?>


  <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
                <?php include('includes/top_nav.php'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <?php include("includes/side_nav.php"); ?>
            <!-- /.navbar-collapse -->
        </nav>
               
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Students
                            <small>add</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                            </li>
                            <li>
                                <i class="fa fa-photo"></i> <a href="users.php">Students</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-plus"></i> <a href="#">Add</a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" id="name" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="enroll_no">Enroll no</label>
                                    <input type="text" name="enroll_no" id="enroll_no" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="exam_no">Exam no</label>
                                    <input type="text" name="exam_no" id="exam_no" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="department_id">Department</label>
                                    <select name="department_id" id="department_id" class="form-control" required>
                                        <option value="">Select a department</option>
                                        <?php
                                            $deps = ['Quran', 'Hadith', 'Fiqh', 'Aqeeda', 'Da\'wa', 'Arabic language']; 
                                            for($i =0; $i <= 5; $i++) : 
                                        ?>
                                            <option value="<?php echo $i + 1; ?>"><?php echo $deps[$i]; ?></option>
                                        <?php endfor; ?>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="submit" name="create" class="btn btn-primary pull-right" value="Create">
                                </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
            
           
            <?php include("includes/footer.php"); ?>
<?php include("includes/header.php"); ?>
<?php if($session->isLoggedIn()){}else{ redirect("login.php"); } ?>
<?php

$msg = "";

if(empty($_GET['id'])){
    redirect("users.php");
}else{
    
    $student = Student::findById($_GET['id']);
    
    if(isset($_POST['update'])){
        if($student){
            $student->name = $_POST['name'];
            $student->enroll_no = $_POST['enroll_no'];
            $student->exam_no = $_POST['exam_no'];
            $student->department_id = $_POST['department_id'];

            if($student->save()) {
                redirect('users.php?success');
            } else {
                redirect('edit_user.php?id='.$_POST['id']);
            }
            
        }
    }
}
?>

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
                <?php include('includes/top_nav.php'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <?php include("includes/side_nav.php"); ?>
            <!-- /.navbar-collapse -->
        </nav>
               
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                <h1 class="page-header">
                    Students
                    <small>Edit</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                    </li>
                    <li>
                        <i class="fa fa-user"></i> <a href="users.php">Students</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-edit"></i> <a href="#">Edit</a>
                    </li>
                </ol>
            </div>
            </div>
            

            <div class="container-fluid">
               <div class="row">
                   <div class="col-md-12">
                       <?php echo $msg; ?>
                   </div>
               </div>
                <!-- Page Heading -->
                        <form action="" method="post" enctype="multipart/form-data">
                            
                            <input type="hidden" name="id" id="id" class="form-control" value="<?php echo $_GET['id']; ?>">
                            
                            <div class="row">
                                <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="text" name="name" id="name" class="form-control" value="<?php echo $student->name; ?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="enroll_no">Enroll no</label>
                                            <input type="text" name="enroll_no" id="enroll_no" class="form-control" value="<?php echo $student->enroll_no; ?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exam_no">Exam no</label>
                                            <input type="text" name="exam_no" id="exam_no" class="form-control"  value="<?php echo $student->exam_no; ?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="department_id">Department</label>
                                            <select name="department_id" id="department_id" class="form-control" required>
                                                <option value="">Select a department</option>
                                                <?php
                                                    $deps = ['Quran', 'Hadith', 'Fiqh', 'Aqeeda', 'Da\'wa', 'Arabic language']; 
                                                    for($i =0; $i <= 5; $i++) : 
                                                ?>
                                                    <option value="<?php echo $i + 1; ?>" <?php echo $student->department_id == $i + 1 ? 'selected' : ''; ?>><?php echo $deps[$i]; ?></option>
                                                <?php endfor; ?>

                                            </select>
                                        </div>
                                        <div class="form-group">
                                           <a href="delete_user.php?id=<?php echo $student->id; ?>" class="btn btn-danger">Delete</a>
                                            <input type="submit" name="update" class="btn btn-primary pull-right" value="Update">
                                        </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

  <?php include("includes/footer.php"); ?>
<?php include("includes/header.php"); ?>
<?php

    if(isset($_GET['id'])){
        $comments = Comment::findComments($_GET['id']);
    }else{
        $comments = Comment::all();
    }
    

?>

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
                <?php include('includes/top_nav.php'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <?php include("includes/side_nav.php"); ?>
            <!-- /.navbar-collapse -->
        </nav>
               
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                <h1 class="page-header">
                    Comments
                    <small>Beta</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-comments-o"></i> <a href="comments.php">Comments</a>
                    </li>
                </ol>
            </div>
            </div>

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Photo</td>
                                    <td>Author</td>
                                    <td>Comment</td>
                                    <td>Date</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if(count($comments) > 0){
                                        $i = 1;
                                        foreach($comments as $comment) : 
                                        $photo = Photo::findById($comment->photo_id);
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><img src="<?php echo $photo->filename !== '' ? $photo->picture_path() : 'http://placehold.it/62x62'; ?>" width="80" alt=""></td>
                                            <td><?php echo $comment->author; ?></td>
                                            <td><?php echo $comment->body; ?></td>
                                            <td><?php echo date('d-m-Y g:i A',strtotime($comment->date)); ?></td>
                                            <td><a href="delete_comment.php?id=<?php echo $comment->id; ?>"><i class="fa fa-trash"></i></a></td>
                                        </tr>
                                    <?php $i++;    endforeach;
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

  <?php include("includes/footer.php"); ?>
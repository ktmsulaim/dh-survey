<?php include("includes/header.php"); ?>
<?php if($session->isLoggedIn()){}else{ redirect("login.php"); } ?>
<?php
    $students = Student::all();
    
    $msg = "";


    if(isset($_GET['success'])){
        $msg = '<div class="alert alert-success"><b>Success! </b>The Students has been deleted.</div>';
    }
?>

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
                <?php include('includes/top_nav.php'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <?php include("includes/side_nav.php"); ?>
            <!-- /.navbar-collapse -->
        </nav>
               
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                <h1 class="page-header">
                    Students
                    <small>Beta</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-user"></i> <a href="users.php">Students</a>
                    </li>
                </ol>
                <div class="text-right" id="btns">
                    <a href="add_user.php" class="btn btn-primary btn-sm"><i class="fa fa-plus fa-fw mr-1"></i>Add student</a>
                </div>
            </div>
            </div>
            <form id="deleteMulti" action="delete_multi.php" method="post">

            </form>
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-md-12">
                        <?php echo $msg; ?>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                    <?php if (count($students) > 0) : ?>
                        <table class="table table-hover dataTable">
                            <thead>
                                <tr>
                                    <td><input type="checkbox" id="checkall"></td>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Enroll.no</th>
                                    <th>Exam no</th>
                                    <th>Department</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $i=1; foreach($students as $student) : 
                                ?>
                                    <tr>
                                        <td><input form="deleteMulti" value="<?php echo $student->id ?>" type="checkbox" name="ids[]" class="check"></td>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $student->name; ?></td>
                                        <td><?php echo $student->enroll_no; ?></td>
                                        <td><?php echo $student->exam_no; ?></td>
                                        <td><?php echo $student->getDep($student->department_id); ?></td>
                                        <td><a href="edit_user.php?id=<?php echo $student->id; ?>"><i class="fa fa-edit fa-fw"></i></a></td>
                                    </tr>
                                
                                <?php $i++; endforeach; ?>
                            </tbody>
                        </table>

                         <?php endif; ?>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

  <?php include("includes/footer.php"); ?>

  <script>
    const main = document.getElementById('checkall');
    const all = document.querySelectorAll('.check');
    let checked;

    const showForm = () => {
        const container = document.getElementById('btns');
        const html = `<button id="deleteBtn" class="btn btn-danger btn-sm" form="deleteMulti" type="submit">Delete</button>`;
        const button = document.getElementById('deleteBtn');
        
        if(button) {
            button.parentElement.removeChild(button);
        }

        container.insertAdjacentHTML('beforeend', html);
    };

    const hideForm = () => {
        const button = document.getElementById('deleteBtn');
        if(button) {
            button.parentElement.removeChild(button);
        }
    };

    main.addEventListener('change', e => {
        let checked = 0;

        if(main.checked) {
            Array.from(all).forEach(cur => {
                cur.checked = 'checked';
            });
            checked = document.querySelectorAll('.check:checked').length;
        } else {
            Array.from(all).forEach(cur => {
                    cur.checked = false;
            });
            checked = document.querySelectorAll('.check:checked').length;
        }

        if(checked > 0) {
            showForm();
        } else {
            hideForm();
        }
    });

    window.addEventListener('load', e => {
        let checked = document.querySelectorAll('.check:checked').length;
        if(checked > 0) {
            showForm();
        } else {
            hideForm();
        }
    });

    document.querySelector('tbody').addEventListener('click', e => {
        checked = document.querySelectorAll('.check:checked').length;
        console.log(checked);
        if(checked > 0) {
            showForm();
        } else {
            hideForm();
        }
        
    });
  </script>
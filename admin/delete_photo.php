<?php include("includes/init.php"); ?>
<?php if($session->isLoggedIn()){}else{ redirect("login.php"); } ?>

<?php

if(empty($_GET['id'])){
    redirect("photos.php");
}

$photo = Photo::findById($_GET['id']);

if($photo){
    if($photo->deletePhoto()){
        redirect("photos.php?delete=success&unlink=success");
    }else{
        redirect("photos.php?delete=success&unlink=false");
    }
    
}else{
    redirect("photos.php");
}

?>
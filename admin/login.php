<?php require_once('includes/header.php'); ?>

<?php

if($session->isLoggedIn()){
    redirect("index.php");
}

$message = "";

if(isset($_POST['login'])){
    $username = trim($_POST['username']);
    $password = trim($_POST['password']);
    
    $userFound = User::verifyUser($username, $password);
    
    if($userFound){
        $session->logIn($userFound);
        redirect("index.php");
    }else{
        $message = "The username or password were incorrect";
    }
}else{
    $username = "";
    $password = "";
   
}

?>


<style>
    body{
        background: #f1f1f1;
    }
    #wrapper{
        padding: 0 !important;
    }
</style>
<div class="container">
<div class="row">
    <div class="col-md-12">
      <h3>Login</h3>  
    </div>
</div>


    <div class="row">
        
        <div class="col-md-4" style="margin:auto">
        <?php if(!empty($message)){ ?>
        <div class="alert alert-danger"><?php echo $message; ?></div>
        <?php } ?>
        <form id="login-id" action="" method="post">

        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" class="form-control" name="username" value="<?php echo htmlentities($username); ?>" >

        </div>

        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" name="password" value="<?php echo htmlentities($password); ?>">

        </div>


        <div class="form-group">
        <input type="submit" name="login" value="Submit" class="btn btn-primary">

        </div>


        </form>


        </div>


    </div>
</div>
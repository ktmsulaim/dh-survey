<?php include("includes/header.php"); ?>
<?php if($session->isLoggedIn()){}else{ redirect("login.php"); } ?>
<?php
    // $photos = ;
    $page = !empty($_GET['p']) ? (int)$_GET['p'] : 1; 
    $per_page = 5;
    $total = count(Photo::all());

    $paginate = new Paginate($page, $per_page, $total);

    $sql = "SELECT * FROM photos LIMIT {$per_page} OFFSET {$paginate->offset()}";
    $photos = Photo::query($sql);
    $msg = "";


    if(isset($_GET['success'])){
        $msg = '<div class="alert alert-success"><b>Success! </b>The photo has been deleted.</div>';
    }
?>

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
                <?php include('includes/top_nav.php'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <?php include("includes/side_nav.php"); ?>
            <!-- /.navbar-collapse -->
        </nav>
               
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                <h1 class="page-header">
                    Photos
                    <small>Beta</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-photo"></i> <a href="photos.php">Photos</a>
                    </li>
                </ol>
            </div>
            </div>

            <div class="container-fluid">

                <!-- Page Heading -->
                
                <?php include("includes/photo_content.php"); ?>
                <!-- /.row -->

                <div class="row">
                    <div class="pages">
                        <ul class="pagination">
                            <?php 
                                if($paginate->hasPrev()){
                                    echo "<li><a href='photos.php?p={$paginate->prev()}'>Prev</a></li>"; 
                                }

                                // number of pages
                                for($i=1; $i <= $paginate->total_page(); $i++){
                                    if($i == $paginate->page){
                                        echo "<li class='active'><a href='photos.php?p={$i}'>{$i}</a></li>";
                                    }else{
                                        echo "<li><a href='photos.php?p={$i}'>{$i}</a></li>";
                                    }
                                }

                                if($paginate->hasNext()){
                                    echo "<li class='next'><a href='photos.php?p={$paginate->next()}'>Next</a></li>"; 
                                }
                            ?>
                        </ul>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

  <?php include("includes/footer.php"); ?>
<?php
require_once('includes/init.php');

if(isset($_GET['id'])){
    $id = $_GET['id'];
    $comment = Comment::findById($id);

    if($comment->delete()){
        redirect("comments.php?success");
    }else{
        redirect("comments.php?failed");
    }
}else{
    redirect("comments.php");
}
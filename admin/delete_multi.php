<?php include("includes/init.php"); ?>
<?php if($session->isLoggedIn()){}else{ redirect("login.php"); } ?>

<?php

if(count($_POST['ids']) <= 0){
    redirect("users.php");
}

foreach($_POST['ids'] as $id) {
    $student = Student::findById($id);

    if($student) {
        $student->delete();
    }
}

redirect("users.php?success");


?>
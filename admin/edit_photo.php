<?php include("includes/header.php"); ?>
<?php if($session->isLoggedIn()){}else{ redirect("login.php"); } ?>
<?php

if(empty($_GET['id'])){
    redirect("photos.php");
}else{
    
    $photo = Photo::findById($_GET['id']);
    
    if(isset($_POST['update'])){
        if($photo){
            $photo->title = $_POST['title'];
            $photo->caption = $_POST['caption'];
            $photo->alt = $_POST['alt'];
            $photo->description = $_POST['desc'];
            
            $photo->save();
        }
    }
}
//    $photos = Photo::all();
?>

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
                <?php include('includes/top_nav.php'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <?php include("includes/side_nav.php"); ?>
            <!-- /.navbar-collapse -->
        </nav>
               
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                <h1 class="page-header">
                    Photos
                    <small>Edit</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                    </li>
                    <li>
                        <i class="fa fa-photo"></i> <a href="photos.php">Photos</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-edit"></i> <a href="#">Edit</a>
                    </li>
                </ol>
            </div>
            </div>

            <div class="container-fluid">

                <!-- Page Heading -->
                    <div class="row">
                        <form action="" method="post">
                        <div class="col-md-8">
                               <div class="form-group">
                                   <a class="thumbnail" href="#">
                                       <img src="<?php echo $photo->picture_path(); ?>" alt="<?php echo $photo->alt; ?>">
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" name="title" id="title" class="form-control" value="<?php echo $photo->title; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="caption">Caption</label>
                                    <input type="text" name="caption" id="caption" class="form-control" value="<?php echo $photo->caption; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="alt">Alternate text</label>
                                    <input type="text" name="alt" id="alt" class="form-control" value="<?php echo $photo->alt; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="editor">Description</label>
                                    <textarea name="desc" id="editor" rows="5" class="form-control"><?php echo $photo->description; ?></textarea>
                                </div>
                        </div>
                        <div class="col-md-4" >
                            <div  class="photo-info-box">
                                <div class="info-box-header">
                                   <h4>Save <span id="toggle" class="glyphicon glyphicon-menu-up pull-right"></span></h4>
                                </div>
                            <div class="inside">
                              <div class="box-inner">
                                 <p class="text">
                                   <span class="glyphicon glyphicon-calendar"></span> Uploaded on: <?php echo !empty($photo->updated_at) ? date('F j,Y', strtotime($photo->updated_at)) . ' @ '.date('h:i', strtotime($photo->updated_at)) : 'Not updated yet.'; ?>
                                  </p>
                                  <p class="text ">
                                    Photo Id: <span class="data photo_id_box"><?php echo $photo->id; ?></span>
                                  </p>
                                  <p class="text">
                                    Filename: <span class="data"><?php echo $photo->filename; ?></span>
                                  </p>
                                 <p class="text">
                                  File Type: <span class="data"><?php echo $photo->type; ?></span>
                                 </p>
                                 <p class="text">
                                   File Size: <span class="data"><?php echo $photo->size; ?></span>
                                 </p>
                              </div>
                              <div class="info-box-footer clearfix">
                                <div class="info-box-delete pull-left">
                                    <a  href="delete_photo.php?id=<?php echo $photo->id; ?>" class="btn btn-danger">Delete</a>   
                                </div>
                                <div class="info-box-update pull-right ">
                                    <input type="submit" name="update" value="Update" class="btn btn-primary ">
                                    </div>   
                                  </div>
                                </div>          
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        </form>
                    </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

  <?php include("includes/footer.php"); ?>
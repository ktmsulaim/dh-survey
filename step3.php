<?php include_once('includes/header.php'); ?>
<?php if($stdsession->isVerified()){}else{ redirect("login.php"); } ?>
<?php

    $student_id = Student::studentByEnroll($_SESSION['enroll_no']);
    $student = Student::findById($student_id);

      // get answer1 from section 2
      $answer2 = Answer::getBySection($student->id, 3);
      $status = 0;
      $answers = array();
      if($answer2->num_rows > 0){
          foreach($answer2 as $ans){
              $answer_number = $ans['answer_number'];
              $answer = $ans['answer'];
  
              array_push($answers, $answer);
              
          }
  
          if(empty($answers[0])){
              $status = 0;
          }else{
              $status = 1;
          }
      }
  
      if($status == 1){
          if(Student::checkAll($student->id) > 0){
              redirect("finish.php");
          }
      }else{
        //   redirect("step3.php");
      }
    
    $answer1 = Answer::answerByStudentId($student->id, 3, 1);
    $answer2 = Answer::answerByStudentId($student->id, 3, 2);
    $answer3 = Answer::answerByStudentId($student->id, 3, 3);

    $ansd1id = "";
    $ansd1 = "";
    $sub = "";

    
    if($answer1->num_rows > 0){
        foreach($answer1 as $ans1){
            $ansd1id = $ans1['id'];
            $ansd1 = $ans1['answer'];
        }
    }

    // Getting sub answers if any 
    if($ansd1 !== 'Job in Mahallu (as khateeb, coordinator, or others )' || $ansd1 !== 'Go for a job abroad'){
       if(!empty($ansd1id)){
        $sub = Sub_answer::getByAnswerId($ansd1id);
       }
        $answers = array();
        
        if($sub){
            foreach($sub as $key => $val){
                array_push($answers, $val->answer);
            }

            if($ansd1 == 'Teacher in DH system' || $ansd1 == 'Teacher outside the system' || $ansd1 == 'Do another job'){
                $sbans1 = $answers[0];
            }elseif($ansd1 == 'Go for higher studies'){
                $sbans1 = $answers[0];
                $sbans2 = $answers[1];
                $sbans3 = $answers[2];
                $sbans4 = $answers[3];
            }else{
                $sbans1 = "";
                $sbans2 = "";
                $sbans3 = "";
                $sbans4 = "";
            }
        }

      

        
        // echo $sbans4;
    }

    if($answer2->num_rows > 0){
        foreach($answer2 as $ans2){
            $ansd2id = $ans2['id'];
            $ansd2 = $ans2['answer'];
        }
    }

    if($answer3->num_rows > 0){
        foreach($answer3 as $ans3){
            $ansd3id = $ans3['id'];
            $ansd3 = $ans3['answer'];
        }
    }

    if(isset($_POST['next'])){
        $answer1 = $_POST['answer1'];
        $answer2 = $_POST['answer2'];
        $answer3 = $_POST['answer3'];

        // inserting answer 1
        if(!empty($answer1) || $answer1 !== ''){
            // checking if already inserted
            if(Student::hasStep3($student->id)){
                // Retrieve the answer 1
                $answer1 = Answer::answerByStudentId($student->id, 3, 1);
                if($answer1->num_rows > 0){
                    foreach($answer1 as $ans1){
                        $ansd1id = $ans1['id'];
                        $ansd1 = $ans1['answer'];
                    }
                }

                if($_POST['answer1'] !== $ansd1){
                    $answer = Answer::findById($ansd1id);
                    $answer->answer = trim($_POST['answer1']);
                    if($answer->save()){
                        $last = $ansd1id;
                        // saving sub answers
                        // Check if already sub answer for this answer id 
                        $sub1 = Sub_answer::getByAnswerId($ansd1id);
                        if($sub1){
                            if(count($sub1) > 0){
                                foreach($sub1 as $sb1){
                                    $sb1->delete();
                                }
                            }
                        }
                        if($_POST['answer1'] == 'Teacher in DH system' || $_POST['answer1'] == 'Teacher outside the system'){
                            // teaching has 1 sub answer
                            $sub_answer1 = new Sub_answer();
                            $sub_answer1->student_id = $student->id;
                            $sub_answer1->answer_id = $last;
                            $sub_answer1->answer = trim($_POST['answer1sub_teach']);
                            $sub_answer1->save();

                        }elseif($_POST['answer1'] == 'Go for higher studies'){
                            // higher study has 3 sub answers
                            // first when
                            $sub_answer2 = new Sub_answer();
                            $sub_answer2->student_id = $student->id;
                            $sub_answer2->answer_id = $last;
                            $sub_answer2->answer = trim($_POST['answer1sub_study1']);
                            $sub_answer2->save();

                            // which subject
                            $sub_answer3 = new Sub_answer();
                            $sub_answer3->student_id = $student->id;
                            $sub_answer3->answer_id = $last;
                            $sub_answer3->answer = trim($_POST['answer1sub_teach2']);
                            $sub_answer3->save();

                            // if subject is others
                            if($_POST['answer1sub_teach2'] == 'others'){
                                $sub_answer4 = new Sub_answer();
                                $sub_answer4->student_id = $student->id;
                                $sub_answer4->answer_id = $last;
                                $sub_answer4->answer = trim($_POST['answer1sub_study2_sub']);
                                $sub_answer4->save();

                            }

                            // which university
                            $sub_answer5 = new Sub_answer();
                            $sub_answer5->student_id = $student->id;
                            $sub_answer5->answer_id = $last;
                            $sub_answer5->answer = trim($_POST['answer1sub_study3']);
                            $sub_answer5->save();


                        }elseif($_POST['answer1'] == 'Do another job'){
                            // has one sub answer
                            // which job you want
                            $sub_answer6 = new Sub_answer();
                            $sub_answer6->student_id = $student->id;
                            $sub_answer6->answer_id = $last;
                            $sub_answer6->answer = trim($_POST['answer1sub_job_sub']);
                            $sub_answer6->save();
                        }
                        
                    }else{
                        redirect("step3.php?failed=answer1");
                    }
                }else{
                    $last = $ansd1id;
                        // saving sub answers
                        // Check if already sub answer for this answer id 
                        $sub1 = Sub_answer::getByAnswerId($ansd1id);
                        if(count($sub1) > 0){
                            foreach($sub1 as $sb1){
                                $sb1->delete();
                            }
                        }
                        if($_POST['answer1'] == 'Teacher in DH system' || $_POST['answer1'] == 'Teacher outside the system'){
                            // teaching has 1 sub answer
                            $sub_answer1 = new Sub_answer();
                            $sub_answer1->student_id = $student->id;
                            $sub_answer1->answer_id = $last;
                            $sub_answer1->answer = trim($_POST['answer1sub_teach']);
                            $sub_answer1->save();

                        }elseif($_POST['answer1'] == 'Go for higher studies'){
                            // higher study has 3 sub answers
                            // first when
                            $sub_answer2 = new Sub_answer();
                            $sub_answer2->student_id = $student->id;
                            $sub_answer2->answer_id = $last;
                            $sub_answer2->answer = trim($_POST['answer1sub_study1']);
                            $sub_answer2->save();

                            // which subject
                            $sub_answer3 = new Sub_answer();
                            $sub_answer3->student_id = $student->id;
                            $sub_answer3->answer_id = $last;
                            $sub_answer3->answer = trim($_POST['answer1sub_teach2']);
                            $sub_answer3->save();

                            // if subject is others
                            if($_POST['answer1sub_teach2'] == 'others'){
                                $sub_answer4 = new Sub_answer();
                                $sub_answer4->student_id = $student->id;
                                $sub_answer4->answer_id = $last;
                                $sub_answer4->answer = trim($_POST['answer1sub_study2_sub']);
                                $sub_answer4->save();

                            }

                            // which university
                            $sub_answer5 = new Sub_answer();
                            $sub_answer5->student_id = $student->id;
                            $sub_answer5->answer_id = $last;
                            $sub_answer5->answer = trim($_POST['answer1sub_study3']);
                            $sub_answer5->save();


                        }elseif($_POST['answer1'] == 'Do another job'){
                            // has one sub answer
                            // which job you want
                            $sub_answer6 = new Sub_answer();
                            $sub_answer6->student_id = $student->id;
                            $sub_answer6->answer_id = $last;
                            $sub_answer6->answer = trim($_POST['answer1sub_job_sub']);
                            $sub_answer6->save();
                        }

                }
            }else{
                $answer = new Answer();
                $answer->student_id = $student->id;
                $answer->section = 3;
                $answer->answer_number = 1;
                $answer->answer = trim($_POST['answer1']);

                if($answer->save()){
                    $last = $db->last_id();
                    // saving sub answers
                    
                    if($_POST['answer1'] == 'Teacher in DH system' || $_POST['answer1'] == 'Teacher outside the system'){
                        // teaching has 1 sub answer
                        $sub_answer1 = new Sub_answer();
                        $sub_answer1->student_id = $student->id;
                        $sub_answer1->answer_id = $last;
                        $sub_answer1->answer = trim($_POST['answer1sub_teach']);
                        $sub_answer1->save();
                    }elseif($_POST['answer1'] == 'Go for higher studies'){
                        // higher study has 3 sub answers
                        // first when
                        $sub_answer2 = new Sub_answer();
                        $sub_answer2->student_id = $student->id;
                        $sub_answer2->answer_id = $last;
                        $sub_answer2->answer = trim($_POST['answer1sub_study1']);
                        $sub_answer2->save();

                        // which subject
                        $sub_answer3 = new Sub_answer();
                        $sub_answer3->student_id = $student->id;
                        $sub_answer3->answer_id = $last;
                        $sub_answer3->answer = trim($_POST['answer1sub_teach2']);
                        $sub_answer3->save();

                        // if subject is others
                        if($_POST['answer1sub_teach2'] == 'others'){
                            $sub_answer4 = new Sub_answer();
                            $sub_answer4->student_id = $student->id;
                            $sub_answer4->answer_id = $last;
                            $sub_answer4->answer = trim($_POST['answer1sub_study2_sub']);
                            $sub_answer4->save();

                        }

                        // which university
                        $sub_answer5 = new Sub_answer();
                        $sub_answer5->student_id = $student->id;
                        $sub_answer5->answer_id = $last;
                        $sub_answer5->answer = trim($_POST['answer1sub_study3']);
                        $sub_answer5->save();


                    }elseif($_POST['answer1'] == 'Do another job'){
                        // has one sub answer
                        // which job you want
                        $sub_answer6 = new Sub_answer();
                        $sub_answer6->student_id = $student->id;
                        $sub_answer6->answer_id = $last;
                        $sub_answer6->answer = trim($_POST['answer1sub_job_sub']);
                        $sub_answer6->save();
                    }
                }
            }
            
        }else{
            redirect("step3.php?answer1=empty");
        }

        // inseting answer 2
        if(!empty($answer2) && $answer2 !== ''){
            if(Student::hasStep3($student->id)){
                $answer2 = Answer::answerByStudentId($student->id, 3, 2);
                if($answer2->num_rows > 0){
                    foreach($answer2 as $ans2){
                        $ansd2id = $ans2['id'];
                        $ansd2 = $ans2['answer'];
                    }
                }

                if($_POST['answer2'] !== $ansd2){
                    $answer = Answer::findById($ansd2id);
                    $answer->answer = trim($_POST['answer2']);
                    $answer->save();
                }
            }else{
                $answer = new Answer();
                $answer->student_id = $student->id;
                $answer->section = 3;
                $answer->answer_number = 2;
                $answer->answer = $_POST['answer2'];
                $answer->save();
            }
        }else{
            redirect("step3.php?answer2=empty");
        }

        // inseting answer 3
        if(!empty($answer3) && $answer3 !== ''){
            if(Student::hasStep3($student->id)){
                $answer3 = Answer::answerByStudentId($student->id, 3, 3);
                if($answer3->num_rows > 0){
                    foreach($answer3 as $ans3){
                        $ansd3id = $ans3['id'];
                        $ansd3 = $ans3['answer'];
                    }
                }

                if($_POST['answer3'] !== $ansd3){
                    $answer = Answer::findById($ansd3id);
                    $answer->answer = trim($_POST['answer3']);
                    
                    if($answer->save()){
                        redirect("step4.php");
                    }else{
                        redirect("step3.php?failed3");
                    }
                }else{
                    redirect("step4.php");
                }
            }else{
                $answer = new Answer();
                $answer->student_id = $student->id;
                $answer->section = 3;
                $answer->answer_number = 3;
                $answer->answer = trim($_POST['answer3']);

                if($answer->save()){
                    redirect("step4.php");
                }else{
                    redirect($_SERVER['PHP_SELF']."?failed");
                }
            }
            
        }else{
            redirect("step3.php?answer3=empty");
        }
    }

?>
    <div class="container m-auto">
        <div class="row">
            <div class="col-md-9 col-sm-10 col-xs-12 m-auto">
                <form action="" method="POST">
                <section class="mb-5">

                    <!-- Card -->
                    <div class="card card-cascade narrower">

                    <!-- Card image -->
                    <div class="view view-cascade text-center bg-white">
                        <h2><br> HUDAWI <sup class="text-danger"><strong>+</strong></sup><br></h2>
                        <div class="progress md-progress mt-3">
                            <div class="progress-bar" role="progressbar" style="width: 50%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <!-- /Card image -->

                    <!-- Card content -->
                    <div class="card-body card-body-cascade text-center table-responsive">

                        <!-- Horizontal Steppers -->
                        <div class="row">
                            <div class="col-md-12">
                                <p class="h5 text-center mb-4 step-head">Step 2</p>


                                <div class="md-form text-left">
                                    <strong>1. What are you going to do next year after graduation? </strong><br><span>(It is compulsory to choose one of the following options, if not planned yet, choose one with more probability)</span>
                                    <div class="answer1">
                                        <?php
                                        $q1 = "";
                                        $q2 = "";
                                        $q3 = "";
                                        $q4 = "";
                                            if(!empty($ansd1)){
                                                if($ansd1 == 'Teacher in DH system' || $ansd1 == 'Teacher outside the system'){
                                                    $q1 = "Term : ";
                                                }elseif($ansd1 == 'Go for higher studies'){
                                                    $q1 = "When : ";
                                                    $q2 = "Subject : ";
                                                    if($sbans2 == 'others'){
                                                        $q3 = "Else : ";
                                                    }
                                                    $q4 = "University : ";
                                                }elseif($ansd1 == 'Do another job'){
                                                    $q1 = 'Else : ';
                                                }



                                                echo '<div class="mt-2">
                                                <div class="alert alert-light" role="alert">
                                                    You have selected : <b class="text-primary">'.$ansd1.'</b><br>
                                                   '.$q1.' '.(!empty($sbans1) ? '<span class="text-primary">'.$sbans1.'</span><br>' : '').'
                                                   '.$q2.' '.(!empty($sbans2) ? '<span class="text-primary">'.$sbans2.'</span><br>' : '').'
                                                   '.$q3.' '.(!empty($sbans3) ? '<span class="text-primary">'.$sbans3.'</span><br>' : '').'
                                                   '.$q4.' '.(!empty($sbans4) ? '<span class="text-primary">'.$sbans4.'</span><br>' : '').'
                                                </div>
                                            </div>';
                                            }
                                        ?>
                                        <select id="answer1" class="mdb-select md-form colorful-select dropdown-primary" name="answer1" required searchable="Search here.." required>
                                            <option value="" selected>Pick an answer</option>
                                            <option value="Teacher in DH system">Teacher in DH system</option>
                                            <option value="Teacher outside the system">Teacher outside the system</option>
                                            <option value="Job in Mahallu (as khateeb, coordinator, or others )">Job in Mahallu (as khateeb, coordinator, or others)</option>
                                            <option value="Go for a job abroad">Go for a job abroad</option>
                                            <option value="Go for higher studies">Go for higher studies</option>
                                            <option value="Do another job">Do another job</option>
                                        </select>
                                        <div id="submenu_teach">
                                            <!-- Material inline 1 -->
                                            <div class="form-check form-check-inline">
                                            <input type="radio" class="form-check-input" id="long" name="answer1sub_teach" value="Long term" checked>
                                            <label class="form-check-label" for="long">Long term (More than five years)</label>
                                            </div>

                                            <!-- Material inline 2 -->
                                            <div class="form-check form-check-inline">
                                            <input type="radio" class="form-check-input" id="short" value="Short time" name="answer1sub_teach">
                                            <label class="form-check-label" for="short">Short time (Compulsory 2 years)</label>
                                            </div>
                                        </div>
                                        <div id="submenu_study">
                                            <div class="option1">
                                                <!-- Material inline 1 -->
                                                <div class="form-check form-check-inline">
                                                <input type="radio" class="form-check-input" id="next_year" name="answer1sub_study1" value="Next year" checked>
                                                <label class="form-check-label" for="next_year">Next year</label>
                                                </div>

                                                <!-- Material inline 2 -->
                                                <div class="form-check form-check-inline">
                                                <input type="radio" class="form-check-input" id="after_a_while" value="After a while" name="answer1sub_study1">
                                                <label class="form-check-label" for="after_a_while">After a while</label>
                                                </div>
                                            </div>
                                            <div class="option2 mt-4">
                                                <span>1.1 The subject you want to study further in:</span>
                                                <div class="select">
                                                    <select id="answer1sub_teach2" class="mdb-select md-form colorful-select dropdown-primary" name="answer1sub_teach2">
                                                        <option value="" selected>Pick an option</option>
                                                        <option value="History">History</option>
                                                        <option value="Islamic Studies">Islamic Studies</option>
                                                        <option value="others">Others</option>
                                                    </select>
                                                    <div id="other_subject">
                                                        <input type="text" name="answer1sub_study2_sub" id="answer1sub_study2sub" class="form-control" placeholder="Which subject you want to study?">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="option3 mt-4">
                                                <span>1.2 The institution where you want to be enrolled:</span>
                                                <div class="select">
                                                     <!-- Material inline 1 -->
                                                    <div class="form-check form-check-inline">
                                                    <input type="radio" class="form-check-input" id="central" name="answer1sub_study3" value="Central universities" checked>
                                                    <label class="form-check-label" for="central">Central universities</label>
                                                    </div>

                                                    <!-- Material inline 2 -->
                                                    <div class="form-check form-check-inline">
                                                    <input type="radio" class="form-check-input" id="answer1sub_study3" value="others" name="answer1sub_study3">
                                                    <label class="form-check-label" for="answer1sub_study3">Others</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="other">
                                            <input type="text" name="answer1sub" id="answer1sub" class="form-control" placeholder="Let us know what is your passion?">
                                        </div>
                                        <div id="other_job">
                                            <input type="text" name="answer1sub_job_sub" id="answer1sub_job_sub" class="form-control" placeholder="Type the job which you want to do!">
                                        </div>
                                    </div>
                                </div>
                                <div class="md-form text-left">
                                    <strong>2. Are you interested in Civil service practices?</strong>
                                    <div class="answer2">
                                        <div class="select">
                                            <!-- Material inline 1 -->
                                            <div class="form-check form-check-inline">
                                            <input type="radio" class="form-check-input" id="answer2_yes" name="answer2" value="Yes" checked>
                                            <label class="form-check-label" for="answer2_yes">Yes</label>
                                            </div>

                                            <!-- Material inline 2 -->
                                            <div class="form-check form-check-inline">
                                            <input type="radio" class="form-check-input" id="answer2_no" name="answer2" value="No">
                                            <label class="form-check-label" for="answer2_no">No</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="md-form text-left">
                                    <strong>3. Are interested in working for Hadia projects out of the state?</strong>
                                    <div class="answer2">
                                        <div class="select">
                                            <!-- Material inline 1 -->
                                            <div class="form-check form-check-inline">
                                            <input type="radio" class="form-check-input" id="answer3_yes" name="answer3" value="Yes" checked>
                                            <label class="form-check-label" for="answer3_yes">Yes</label>
                                            </div>

                                            <!-- Material inline 2 -->
                                            <div class="form-check form-check-inline">
                                            <input type="radio" class="form-check-input" id="answer3_no" name="answer3" value="No">
                                            <label class="form-check-label" for="answer3_no">No</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center mt-4">
                                    <a href="step2.php" class="btn btn-primary waves-effect waves-light" name="next">Back</a>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light" name="next">Next</button>
                                </div>
                            </div>
                        </div>
                        <!-- Horizontal Steppers -->

                    </div>
                    <!-- Card content -->

                    </div>
                    <!-- Card -->

                    </section>
                </form>
            </div>
        </div>
    </div>
 

<!-- footer -->
<?php include_once('includes/footer.php'); ?>
<script>
    $(function(){
        $('#other, #submenu_teach, #submenu_study, #other_subject, #other_job').hide();
        $('#answer1').change(function(){
            var val = $(this).val();

            if(val == 'Teacher in DH system' || val == 'Teacher outside the system'){
                $('#submenu_teach').fadeIn();
            }else{
                $('#other, #submenu_teach').fadeOut();
            }

           if(val == 'Go for higher studies'){
                $('#submenu_study').fadeIn();

                $('#answer1sub_teach2').change(function(){
                    var subject = $(this).val();

                    if(subject == 'others'){
                        $('#other_subject').fadeIn();
                    }else{
                        $('#other_subject').fadeOut();
                    }
                });
            }else{
                $('#submenu_study').fadeOut();
            }

            if(val == 'Do another job'){
                $('#other_job').fadeIn();
            }else{
                $('#other_job').fadeOut();
            }
        });
    });
</script>
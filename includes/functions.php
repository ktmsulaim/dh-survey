<?php

function autoLoader($class) {
    $class = strtolower($class);
    $path = "includes/classes/{$class}.php";
    
    if(file_exists($path)){
        require_once($path);
    }else{
        die("The file {$class}.php not found.");
    }
}

spl_autoload_register('autoLoader');

function redirect($location){
    header("Location: {$location}");
}


?>
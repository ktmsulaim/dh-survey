<?php

defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);

define('SITE_ROOT', 'C:' . DS . 'laragon' . DS . 'www' . DS . 'survey');

define('INCLUDES_PATH', SITE_ROOT . DS . 'includes');

define('CLASSES_PATH', SITE_ROOT . DS . 'classes');

require_once('functions.php');    
require_once('config.php');
require_once('database.php');
require_once('classes/db_object.php');
require_once('classes/session.php');
require_once('classes/student.php');
require_once('classes/student_session.php');
require_once('classes/institution.php');
require_once('classes/answer.php');
require_once('classes/sub_answer.php');
require_once('classes/skill.php');
require_once('classes/feedback.php');

?>
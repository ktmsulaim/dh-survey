<?php include_once('includes/header.php'); ?>
<?php

    // verify user
    if($stdsession->isVerified()){
		redirect("index.php");
	}
	
	$message = "";
	
	if(isset($_POST['signin'])){
		$enroll_no = trim($_POST['enroll_no']);
		$exam_no = trim($_POST['exam_no']);
		
		$studentFound = Student::verifyStudent($enroll_no, $exam_no);
		
		if($studentFound){
			$stdsession->verify($studentFound);
			redirect("index.php");
		}else{
			$message = "The student was'nt found";
		}
	}else{
		$enroll_no = "";
	}

?>
    <div class="container m-auto">
        <div class="row">
            <div class="col-md-5 col-sm-10 col-xs-12 m-auto">
                <form action="" method="POST">
                <section class="mb-5">

                    <!-- Card -->
                    <div class="card card-cascade narrower">

                    <!-- Card image -->
                    <div class="view view-cascade text-center bg-white">
                       <!-- <img src="img/DH_LOGO.jpg" width="150" alt="Darul Huda Islamic University" class="m-auto"> -->
                       <h2><br> HUDAWI <sup class="text-danger"><strong>+</strong></sup><br><br></h2>
                    </div>
                    <!-- /Card image -->

                    <!-- Card content -->
                    <div class="card-body card-body-cascade text-center table-responsive">

                        <!-- Horizontal Steppers -->
                        <div class="row">
                            <div class="col-md-12">
                                <p class="h5 text-center mb-4 step-head">Sign in</p>

                                <div class="md-form">
                                    <input type="number" min="0" id="enroll_no" name="enroll_no" class="form-control" required>
                                    <label for="enroll_no" class="">Enroll.No</label>
                                </div>
                                <div class="md-form">
                                    <input type="text" id="exam_no" name="exam_no" class="form-control" required>
                                    <label for="exam_no" class="">Exam no</label>
                                </div>

                                <div class="text-center mt-4">
                                    <button class="btn btn-primary waves-effect waves-light" name="signin">Sign in</button>
                                </div>
                            </div>
                        </div>
                        <!-- Horizontal Steppers -->

                    </div>
                    <!-- Card content -->

                    </div>
                    <!-- Card -->

                    </section>
                </form>
            </div>
        </div>
    </div>
 

<!-- footer -->
<?php include_once('includes/footer.php'); ?>
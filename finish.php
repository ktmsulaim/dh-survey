<?php include_once('includes/header.php'); ?>
<?php if($stdsession->isVerified()){}else{ redirect("login.php"); } ?>
<?php

    $student_id = Student::studentByEnroll($_SESSION['enroll_no']);
    $student = Student::findById($student_id);

?>
    <div class="container m-auto">
        <div class="row">
            <div class="col-md-9 col-sm-10 col-xs-12 m-auto">
                <form action="" method="POST">
                <section class="mb-5">

                    <!-- Card -->
                    <div class="card card-cascade narrower">

                    <!-- Card image -->
                    <div class="view view-cascade text-center bg-white">
                        <h2><br> HUDAWI <sup class="text-danger"><strong>+</strong></sup><br></h2>
                        <div class="progress md-progress mt-3">
                            <div class="progress-bar" role="progressbar" style="width: 100%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <!-- /Card image -->

                    <!-- Card content -->
                    <div class="card-body card-body-cascade text-center table-responsive">

                        <!-- Horizontal Steppers -->
                        <div class="row">
                            <div class="col-md-12">
                                <p class="h5 text-center mb-4 step-head">Congrats</p>


                                <div class="md-form">
                                    <h5>By now, the survey under 24th batch coordination ends.
                                        <br>Insha’Allah, Let us use the coming days to make our dreams come true.
                                        <br>Thanks
                                    </h5>
                                </div>
                                <div class="md-form">
                                    <a href="view.php" class="btn btn-info btn-sm">View</a>
                                    <a href="logout.php" class="btn btn-primary btn-sm">Sign out</a>
                                </div>
                            </div>
                        </div>
                        <!-- Horizontal Steppers -->

                    </div>
                    <!-- Card content -->

                    </div>
                    <!-- Card -->

                    </section>
                </form>
            </div>
        </div>
    </div>
 

<!-- footer -->
<?php include_once('includes/footer.php'); ?>
